/**
 * Простой селект
 *
 *
 */

require('./style.scss');

import React from 'react';
import Utils from '../../utils/utils';
import Api from '../../utils/';
import update from 'immutability-helper';
const api = new Api();

export default class Select extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            item: {
                config: [],
                outjson: [],
                filter: []
            }
        });
    }
    componentDidMount() {
        let self = this;
        const { name } = this.props;
        if(this.props.source) {
            api.get(this.props.source, {}, {
                s: (res) => {
                    self.setState({
                        item: {
                            config: res.data.config,
                            outjson: res.data.outjson,
                            filter: res.data.outjson
                        }
                    });
                }
            });
        }

        /*
            скрыть блок если клик был не по нему
        */
        var _drop = document.querySelector(`#${name}`);
        function removeTest(e) {
            if(!e.target.matches('.yform, .yform *, .delete_btn'))
                if(_drop.classList.contains('visible') && _drop.classList.contains('opacity')) {
                    _drop.classList.toggle('visible');
                    _drop.classList.toggle('opacity');
            }
        };

        window.addEventListener('click', removeTest);
    }
    handleClickOptionVisible() {
        const { name, value, multy } = this.props;

        if(name) {
            let _drop = document.getElementById(name);
            let options = _drop.getElementsByClassName('option').length;
            let _active = _drop.getElementsByClassName('option active');
            _drop.classList.toggle('visible');
            _drop.classList.toggle('opacity');
        }
    }
    handleClickOption(ev) {
        const { name, value, multy } = this.props;
        let _value = ev.target.dataset.value;

        if(name) {
            if(_value !== 'placeholder') {
                if(multy) {
                    let new_arr = new Array;
                    new_arr = value;
                    new_arr.push(parseInt(_value));
                    this.props.selected(new_arr);
                } else {
                    let _drop = document.getElementById(name);
                    let options = _drop.getElementsByClassName('option').length;
                    let _active = _drop.getElementsByClassName('option active');
                    _drop.classList.toggle('visible');
                    _drop.classList.toggle('opacity');
                    this.props.selected(parseInt(_value));
                }
            }
        }
    }
    hundleRemove(ev) {
        const { name, value, multy } = this.props;
        let new_arr = new Array();
        new_arr = value;
        let index = new_arr.indexOf(parseInt(ev.target.dataset.value));
        new_arr.splice(index, 1);
        this.props.selected(new_arr);
    }
    handlerSearch(ev) {
      let value = ev.target.value.toUpperCase();
      let arr = [];
      this.state.item.outjson.map((el)=>{
        if(el.orgname.toUpperCase().indexOf(value) + 1) {
          arr.push(el);
        }
      })
      if(arr.length > 0) {
        this.setState({
          item: update(this.state.item, {
            filter: { $set: arr }
          })
        })
      } else {
        if(value.length > 0) {
          this.setState({
            item: update(this.state.item, {
              filter: { $set: arr }
            })
          })
        } else {
          this.setState({
            item: update(this.state.item, {
              filter: { $set: this.state.item.outjson }
            })
          })
        }
      }
    }
    render() {
        const { label, name, id, value, multy } = this.props;
        return (
            <div className='yform drop ui cointainer' id={name ? name : null}>
                <div className='drop_content'>
                    {
                        multy ?
                            <div className='option_multy active placeholder'
                                data-value='placeholder'>
                                { Array.isArray(value) ?
                                        value.length > 0 ?
                                            value.map((el,i)=>{
                                                return <span key={`${i}sd`} className='item'>
                                                    {el}
                                                </span>
                                            })
                                    : 'Выберите'
                                    : 'Выберите'
                                }
                                <span
                                    className='selector_arrow'
                                    onClick={ev=>this.handleClickOptionVisible(ev)}
                                    dangerouslySetInnerHTML={{__html: '&#9660'}}/>
                            </div>
                            :
                            <div className='option active placeholder'
                                data-value='placeholder'
                                onClick={ev=>this.handleClickOptionVisible(ev)}>
                                    {
                                        value ?
                                            this.state.item.outjson.map(el=>{
                                                if(el[id] === value) return el[name]
                                            })
                                        : 'Выберите'
                                    }
                            </div>
                    }
                    <div className='option'>
                      <div className="ui input focus">
                        <input type="text" onChange={(ev)=>this.handlerSearch(ev)} placeholder="Поиск..." />
                      </div>
                    </div>
                    {
                        this.state.item.filter.map(el=>{
                            if(Array.isArray(value)) {
                                if(value.length > 0) {
                                    if(value.indexOf(el.id) < 0) {
                                        return <div key={el[id] + 'dropitem' + label}
                                                    className='option'
                                                    onClick={ev=>this.handleClickOption(ev)}
                                                    data-value={ el.id }>
                                                        { el[name] }
                                                </div>
                                    } else {
                                        return <div key={el[id] + 'dropitem' + label}
                                                    className='option'
                                                    data-value={ el.id }>
                                                        { el[name] }
                                                        <span
                                                            className='delete_btn'
                                                            data-value={el.id}
                                                            onClick={ev=>this.hundleRemove(ev)}
                                                            dangerouslySetInnerHTML={{__html: '&#215'}} />
                                                </div>
                                    }
                                } else {
                                    return <div key={el[id] + 'dropitem' + label}
                                            className='option'
                                            onClick={ev=>this.handleClickOption(ev)}
                                            data-value={ el.id }>
                                                { el[name] }
                                        </div>
                                }
                            } else {
                                return <div key={el[id] + 'dropitem' + label}
                                            className='option'
                                            onClick={ev=>this.handleClickOption(ev)}
                                            data-value={ el.id }>
                                                { el[name] }
                                        </div>
                            }
                        })
                    }
                </div>
            </div>
        )
    }
}
