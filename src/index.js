import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

// import 'alertifyjs/build/alertify.min';

import '../semantic/out/semantic.min';
import 'jquery-typeahead/dist/jquery.typeahead.min';
import 'jquery-typeahead/dist/jquery.typeahead.min.css';

import './style.scss';

import App from './components/app';
import DashLayout from './components/dash/layout';
import Template from './components/dash/template';
import DashIndex from './components/dash/home';

import ListFormTest from './components/admin/listFormTest';
import Profile from './components/admin/myProfile';
import Notys from './blocks/notys/index';

import configureStore from './redux/configureStore';
const store = configureStore();

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
    <Route {...rest} render={props =>
        <Layout servicename={props.match.params.servicename ? props.match.params.servicename : null}>
            <Component {...props} />
        </Layout>
    } />
);

const Bar404 = () => (
    <div>
        <h3>Ошибка</h3>
        <span>
            Сервис вам недоступен или такого сервиса нет.
        </span>
    </div>
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
        <Switch>
            <Route exact path='/' component={App}/>
            <Route exact path='/ListFormTest' component={ListFormTest}/>
            <AppRoute exact path='/dashboard' layout={DashLayout} component={DashIndex} />
            <AppRoute exact path='/dashboard/:servicename' layout={DashLayout} component={Template} />
            <AppRoute exact path='/dashboard/:servicename/:compname' layout={DashLayout} component={Template} />
            <AppRoute exact path='/dashboard/:servicename/:compname/:id' layout={DashLayout} component={Template} />
            <AppRoute exact path='/dashboard/404' layout={DashLayout} component={Bar404} />
            <AppRoute exact path='/profile' layout={DashLayout} component={Profile} />
            <AppRoute exact path='/notys' layout={DashLayout} component={Notys} />
            <Redirect exact from="/" to='/dashboard' />
        </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
module.hot.accept();