import axios from 'axios';
import { hashHistory } from 'react-router';
import Utils from '../utils/utils';

const BASE_URL = 'http://api.jelata.tech/';

export default class ApiFactory {
    constructor() {
        axios.defaults.withCredentials = true;
    }

    toLogin(){
      let s = `${BASE_URL}atf/auth`;
      location.href = s;
    }

    myShowError(error) {
        let msg = '';
        if (error && error.response && error.response.data.message)
            msg = error.response.data.message;
        else msg = 'Неизвестная ошибка, обратитесь к админинстратору';

        Utils.showErr(msg);
    }

    myHandleError(error) {
        if(error.response) {
            switch (error.response.status) {
                case 400:
                    this.myShowError(error);
                    break;
                case 401: {
                      this.toLogin();
                    }
                    break;
                case 500:
                    this.myShowError(error);
                    break;
                default:
                    hashHistory.push('/error/' + JSON.stringify(error.response.data));
                    break;
            }
        }
        this.myShowError(error);
    }

    post(url, data, callbacks, text) {
        axios.post(BASE_URL + url, data, {
            onUploadProgress: (ProgressEvent)=>{
                if (callbacks && callbacks.progress) {
                    callbacks.progress(ProgressEvent);
                }
            }
        }).then(res => {
                switch (res.status) {
                    case 200:
                        if (callbacks && callbacks.s) {
                            callbacks.s(res);
                        }
                        Utils.showOk(text || 'успешно');
                        break;
                    default:
                        // console.log(res);
                        break;
                }
            })
            .catch(error => {
                if (callbacks && callbacks.e)
                    callbacks.e(error);
                else {
                    this.myShowError(error);
                }
            });
    }

    fetchSprav(config) {
        const {url, statePropName, parent, callback, options}=config;

        let arrName = (options && options.arrName) ? options.arrName : 'outjson';

        this.get(url, {}, {
            s: (ajd) => {

                if (options && options.unshiftWith) {
                    ajd.data[arrName].unshift(options.unshiftWith);
                }
                parent.setState({
                    [`${statePropName}`]: ajd.data
                }, callback)
            }
        });
    }

    get(url, data, callbacks) {
        axios.get(BASE_URL + url, {
                params: data
            })
            .then(res => {
                switch (res.status) {
                    case 200:
                    case 304:
                        if (callbacks && callbacks.s)
                            callbacks.s(res);
                        break;
                    default:
                        //// console.log(res);
                        // if (callbacks && callbacks.e)
                        //     callbacks.e(res);
                        // else{
                        //     showErr(res.error.message);
                        // }
                        break
                }
            })
            .catch(error => {
                if (callbacks && callbacks.e)
                    callbacks.e(error);
                else {
                    this.myHandleError(error);
                }
            });
    }

    put(url, data, callbacks) {
        axios.put(BASE_URL + url, data)
            .then(res => {
                switch (res.status) {
                    case 200:
                        if (callbacks && callbacks.s) {
                            Utils.showOk('Успешно');
                            callbacks.s(res);
                        }
                        break;
                    default:
                        // console.log(res);
                        break;
                }
            })
            .catch(error => {
                if (callbacks && callbacks.e)
                    callbacks.e(error);
                else {
                    this.myHandleError(error);
                }
            });
    }

    delete(url, callbacks) {
        axios.delete(BASE_URL + url)
            .then(res => {
                switch (res.status) {
                    case 200:
                        if (callbacks && callbacks.s)
                            callbacks.s(res);
                        Utils.showOk('Удалено');
                        break;
                    default:
                        null;
                        break;
                }
            })
            .catch(error => {
                if (callbacks && callbacks.e)
                    callbacks.e(error);
                else {
                    this.myHandleError(error);
                }
            });
    }
}