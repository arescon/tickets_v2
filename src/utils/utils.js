import alertifyjs from 'alertifyjs';
import '../../alertify/css/alertify.min.css';
import '../../alertify/css/themes/semantic.min.css';

module.exports = {
    showErr: (msg,delay) => {
        //debugger;
        if(delay){
            alertifyjs.error('Произошла ошибка: ' + msg, delay);
        } else {
            alertifyjs.error('Произошла ошибка: ' + msg);
        }
    },

    showOk: (msg) => {
        alertifyjs.success(msg);
    },

    isEmptyObject: (obj) => {
        for (let i in obj) {
            if (obj.hasOwnProperty(i)) {
                return false;
            }
        }
        return true; // true значит пустой
    },

    showConfirm: (title,msq,ok,no) => {
        alertifyjs.confirm(title,'Вы собираетесь удалить: ' + msq,
            function(){
                ok();
            },
            function(){
                no ? no() : null
            }
        ).set('labels', {ok:'Подтвердить', cancel:'Отмена'});
    },

    ObjectUtils: {
        //удаляет пустые значения из объекта
        removeEmpty: (item) => {
            let _item = {};

            let k = Object.keys(item);

            k.map(i => {
                if (item[i] != '' && item[i] != null && item[i] != undefined)
                    _item[i] = item[i];
            });

            return _item;
        },

        //удаляет ключи
        removeKeys: (item, keys) => {
            keys.forEach(i => {
                if (item[i] != null)
                    delete(item[i]);
            });

            return item;
        },

        safeParam: (o, paramName, def) => {
            if (o[paramName] != null)
                return o[paramName];
            else
                return def;
        }
    },

    UriUtils: {
        //парсит строку ?a=1&b=2 -> {a:1, b:2}
        parseQueryString(s){
            let p = s.indexOf('?');
            let q = (p == -1) ? s : s.substr(p + 1);

            let params = q.split('&');
            let result={};

            let i=0;
            while (i < params.length) {
                let parameter = params[i].split('=');
                result[parameter[0]] = parameter[1];
                i++;
            }

            return result;
        }
    },

    /**
     * Нахождение индекса элемента в массиве объектов по его значению
     *
     * @param {array} arraytosearch массив, используемый для поиска.
     * @param {string} key имя ключевого элемента.
     * @param {string} valuetosearch значение для поиска.
     * @return {i} индекс искомого объекта, при неудачном поиске - null.
     */
    findIndexByKeyValue: (arraytosearch, key, valuetosearch) => {
        for (let i = 0; i < arraytosearch.length; i++) {
            if (arraytosearch[i][key] == valuetosearch) {
                return i;
            }
        }
        return null;
    },
    /**
     * Нахождение индекса элемента в массиве объектов по его значению
     *
     * @param {array} arraytosearch массив, используемый для поиска.
     * @param {string} obj объект для поиска.
     * @return {i} индекс искомого объекта, при неудачном поиске - null.
     */
    findIndexByObj: (arraytosearch, obj) => {
        for (let i = 0; i < arraytosearch.length; i++) {
            if (arraytosearch[i] == obj) {
                return i;
            }
        }
        return null;
    },

    /**
     * преобразование файла в base64
     * @param {file} file 
     * @param {callback} cb 
     */
    getBase64: (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result);
        };
        reader.onerror = function (error) {
            // console.log('Error: ', error);
        };
    },

    /** 
     *  Will remove all falsy values: undefined, null, 0, false, NaN and "" (empty string)
     * 
     *  @param {undefined, null, 0, false, NaN, ""} что удалять
     */
    cleanArray: (actual) => {
        var newArray = new Array();
        for (var i = 0; i < actual.length; i++) {
            if (actual[i]) {
                newArray.push(actual[i]);
            }
        }
        return newArray;
    }
};

