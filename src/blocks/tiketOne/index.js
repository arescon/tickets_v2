import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import update from 'immutability-helper';
import Checkbox from './../globals/CheckForm';
import TableWCP from './../../components/dash/TableWCP';
import Search from './../globals/Search';
import Select from './../globals/selectForm';
import FilePickerBlock from './../globals/filepicker';

import Api from '../../utils/';
const api = new Api();
const prefix = 'api';

/**
 * Преобразование даты в вид 21 февраля 2018;
 * @param date дата в формате 2018-02-27T14:43:51.352953
 * @param watch (true, false) показать время или нет
 * 
 * @returns дата в формате 27 февраля 2018 время (14:26)
 */
function dateFormater(date, watch) {
    let c = new Date(date);
    let d = [];
    var o = {};
    let dc = new Date();
    if(watch){
        o = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            timezone: 'UTC',
            hour: 'numeric',
            minute: 'numeric'
        };
        d.y = c.getFullYear();
        d.m = c.getMonth();
        d.d = c.getDate();
        d.h = c.getHours();
        d.min = c.getMinutes();
        dc = new Date(d.y, d.m, d.d, d.h, d.min);
    } else {
        o = {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        d.y = c.getFullYear();
        d.m = c.getMonth();
        d.d = c.getDate();
        let dc = new Date(d.y, d.m, d.d);
    }

    return dc.toLocaleString('ru', o);
}

export default class TiketOne extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item,
            files: {
                items: []
            },
            redirect: false,
            redirectUrl: ''
        };
        this.props.initItems(this.props.method+'?id='+this.props.id);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({item: nextProps.item});
    }
    
    handleChange(e) {
        let type = e.target.dataset.type;

        this.setState({
            item: update(this.state.item, {
                [`${type}`]: { $set: e.target.value }
            })
        })
    }

    handleFiles(e) {
        let arr = Array.from(e);
        this.setState({
            files: update(this.state.files, {
                items: { $push: arr }
            })
        })
    }

    handleDeleteFile(el) {
        this.setState({
            files: update(this.state.files, {
                items: { $splice: [[parseInt(el.target.dataset.id), 1]] }
            })
        })
    }

    handleSendFrom() {
        let t = new FormData();

        let keys = Object.keys(this.state.item);

        keys.map( el => {
            if(el !== 'files') t.append(el, this.state.item[el]);
        })

        if(Array.isArray(this.state.files.items) && this.state.files.items.length > 0){
            this.state.files.items.forEach((file, i)=>{
                t.append(`files`, file);
            })
        }

        api.post(`fls/${this.props.method}`, t, {
            s: ()=>{
                this.setState({
                    redirect: true
                })
            }
        });
    }

    render() {
        if (this.state.redirect) return <Redirect to={`/dashboard/${this.props.params.servicename}`}/>;
        let { config } = this.props;
        if(this.state.item ) {
            return [
                <div className='field ui form'>
                    <div className='tiket ui segment'>
                        <div className='fields'>
                            <div className='ten wide field'>
                                <label>Тема</label>
                                <input  type='text'
                                        value={ this.state.item ? this.state.item.title ? this.state.item.title : '' : '' }
                                        data-type='title'
                                        onChange={(e)=>this.handleChange(e)}
                                        placeholder='Тема' />
                            </div>
                        </div>
                        <div className='fields'>
                            <div className='five wide field'>
                                <label>Приложение</label>
                                <Select label={ this.state.item ? this.state.item.appname ? this.state.item.appname : '' : '' }
                                        source='api/apps'
                                        name='appname'
                                        id='id'
                                        value={ this.state.item ? this.state.item.appid ? this.state.item.appid : '' : '' }
                                        selected={ id => {
                                            this.setState({
                                                item: update(this.state.item,
                                                    {
                                                        appid: {$set: parseInt(id)}
                                                    }
                                                )
                                            })
                                        }}/>
                            </div>
                            <div className='five wide field'>
                                <label>Организация</label>
                                <Select label={ this.state.item ? this.state.item.orgname ? this.state.item.orgname : '' : '' }
                                        source='api/userorgs'
                                        name='orgname'
                                        id='id'
                                        value={ this.state.item ? this.state.item.orgid ? this.state.item.orgid : '' : '' }
                                        selected={ id => {
                                            this.setState({
                                                item: update(this.state.item,
                                                    {
                                                        orgid: {$set: parseInt(id)}
                                                    }
                                                )
                                            })
                                        }}/>
                            </div>
                        </div>
                        <div className='field'>
                            <label>Сообщение</label>
                            <textarea   row={3}
                                        data-type='mestext'
                                        onChange={(e)=>this.handleChange(e)}
                                        value={ this.state.item ? this.state.item.mestext ? this.state.item.mestext : '' : '' }/>
                        </div>
                        <div className='field'>
                            <label>Файлы</label>
                            <FilePickerBlock
                              buttonStyle='ui icon button grey basic'
                              icon='paperclip'
                              files={this.state.files.items}
                              showFiles={this.state.item.files}
                              handleDelete={ev=>this.handleDeleteFile(ev)}
                              change={ev=>this.handleFiles(ev)} />
                        </div>
                    </div>
                </div>,
                <div className='ui segment'>
                    <button onClick={()=>this.handleSendFrom()} className='ui button green'>Сохранить</button>
                </div>
            ]
        } else return null;
    }
}