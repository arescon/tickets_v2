import React from 'react';
import update from 'immutability-helper';
import { Link, Redirect } from 'react-router-dom';
import ApiFactory from './../../utils/index';
import Checkbox from './../globals/CheckForm';
import TableWCP from './../../components/dash/TableWCP';
import Search from './../globals/Search';

const api = new ApiFactory();
const prefix = 'api';
/**
 * Компонент Profile, есть только изменение ролей, приложений и  организаций
 * 
 * 
 */

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item,
            roles: {
                config: {},
                outjson: []
            }
        };
        this.props.initItems(this.props.method+'?id='+this.props.id);
    }

    componentDidMount(){
        api.fetchSprav({
            url: 'api/roles',
            statePropName: 'roles',
            parent: this,
            callback: () => {
                $('#' + this.id + ' .ui.checkbox').checkbox().on('change', (e) => {
                    this.onRoleChange(e);
                });
            }
        });
    }

    onRoleChange(e) {
        let id = + e.target.dataset.id;

        let idx = this.state.item.roles.indexOf(id);
        let r = this.state.item.roles;

        if (idx >= 0) {
            r.splice(idx, 1);
        } else {
            r.push(id);
        }

        this.setState({
            item: update(this.state.item, {roles: {$set: r}})
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({item: nextProps.item})
    }

    changeValue(e) {
        let mode = e.currentTarget.dataset.mode;
        let type = e.currentTarget.dataset.type;
        let s = e.target.value;
        this.setState({
            item: update(this.state.item,
                {   config: {
                        [`${type}`]: {
                            [`${mode}`]: {$set: s}
                        }
                    }
                }
            )
        })
    }

    changeName(e) {
        let mode = e.currentTarget.dataset.mode;
        let s = e.target.value;

        this.setState({
            item: update(this.state.item,
                { [`${mode}`]: {$set: s} }
            )
        })
    }

    saveChanges(e) {
        e.preventDefault();
        this.props.saveItem(this.state.item, this.props.method);
    }

    handlerActions(el) {
        let action = JSON.parse(el.currentTarget.dataset.el);
        let id = parseInt(el.currentTarget.dataset.id);
        switch (action.method) {
            case 'POST':
                // console.log('post');
                break;
            case 'PUT':
                // console.log('put');
                break;
            case 'DELETE': {
              let index = this.state.item.orgs.indexOf(id)
              let index2;
              this.state.item.orgsjson.map((el,i)=>{
                if(el.id === id) return index2 = i;
              })
              this.setState({
                item: update(this.state.item,
                  {
                    ['orgs']: { $splice: [[index, 1]] },
                    ['orgsjson']: { $splice: [[index2, 1]] }
                  }
                )
              })
              break;
            };
        }
    }

    handleSendForm(e) {
        api.post(`api/user`, {
            id: this.state.item.id ? this.state.item.id : [],
            orgs: this.state.item.orgs ? this.state.item.orgs : [],
            roles: this.state.item.roles ? this.state.item.roles : [],
            apps: this.state.item.apps ? this.state.item.apps : []
        }, {
            s: ()=>{
                this.setState({
                    redirect: true
                })
            }
        });
    }

    render() {
        let t = this.state.item ? this.state.item.config ? Object.keys(this.state.item.config) : [] : [];
        // console.log(this.state);
        if (this.state.redirect) return <Redirect to={`/dashboard/${this.props.params.servicename}`}/>;
        return (
            <div className='ui form segment'>
                <div className='field'>
                    <h2 className='ui header'>{
                        this.state.item ? `${this.state.item.fam} ${this.state.item.im} ${this.state.item.ot}` : ''
                    }
                    </h2>
                    <br/>
                </div>
                <div className='fields'>
                    <div className='six wide field'>
                        <h4 className='ui header'>Роли</h4>
                        <Checkbox
                            label='roles'
                            source='api/roles'
                            name='rolename'
                            dicid='id'
                            value={this.state.item ? this.state.item.roles ? this.state.item.roles : [] : []}
                            checked={(result) => {
                                this.setState({
                                    item: update(this.state.item,
                                        {
                                            ['roles']: { $set: result }
                                        }
                                    )
                                })
                            }}
                        />
                        <br/>
                    </div>
                    <div className='ten wide field'>
                        <h4 className='ui header'>Приложения</h4>
                        <Checkbox
                            label='roles'
                            source='api/apps'
                            name='appname'
                            dicid='id'
                            value={this.state.item ? this.state.item.apps ? this.state.item.apps : [] : []}
                            checked={(result) => {
                                this.setState({
                                    item: update(this.state.item,
                                        {
                                            ['app']: { $set: result }
                                        }
                                    )
                                })
                            }}
                        />
                        <br/>
                    </div>
                </div>
                <div className='field'>
                        <h4 className='ui header left floated'>Организации</h4>
                        <div style={{ width: '400px', float: 'right' }}>
                            <label>Добавить организацию</label>
                            <Search
                                urlS='api/dicsearch?tagname=orgs&substr='
                                name='org'
                                resultS='outjson'
                                titleS='orgname'
                                descriptionS=''
                                styleS={{width: '100%'}}
                                placeHolder='Добавить организацию по поиску'
                                selected={(result) => {
                                    this.setState({
                                        item: update(this.state.item,
                                            {
                                                ['orgs']: { $push: [result.id] },
                                                ['orgsjson']: { $push: [{id : result.id,orgname : result.orgname}] }
                                            }
                                        )
                                    })
                                }}
                            />
                        </div>
                        <br/>
                        <br/>
                        <TableWCP
                            items={ this.state.item ? this.state.item.orgsjson ? this.state.item.orgsjson : [] : [] }
                            config={ {
                                id: {
                                    dicapi: '',
                                    dicid: '',
                                    dictitle: '',
                                    istitle: '',
                                    orderby: '0', 
                                    title: '№',
                                    type: 'text',
                                    union: '',
                                    visibility: 'true'
                                },
                                orgname: {
                                    dicapi: '',
                                    dicid: '',
                                    dictitle: '',
                                    istitle: '',
                                    orderby: '0', 
                                    title: 'Название',
                                    type: 'text',
                                    union: '',
                                    visibility: 'true'
                                }
                            } }
                            // method={ 'dicsel?tagname=orgs' }
                            // initItems={ (string)=> this.initItem(string) }
                            // doubleUrl={el.config ? el.config.double_url ? el.config.double_url : null : null }
                            // currentLocation ={`/dashboard/${servname}`}
                            actions={[
                                {
                                    icon: 'trash',
                                    id: 1,
                                    method: 'DELETE',
                                    name: 'Удалить',
                                    tag: 'orgs'
                                }
                            ]}
                            actionFunc={(el)=> this.handlerActions(el)}
                            // rowDoubleClick={ (el)=> this.doubleClick(el) }
                        />
                        <br/>
                        <br/>
                    </div>
                <div className='field'>
                    <button
                        className='ui green button'
                        onClick={e=>this.handleSendForm(e)} >
                        Сохранить
                    </button>
                    <button className='ui grey button'>
                        Отмена
                    </button>
                </div>
            </div>
        )
    }
};