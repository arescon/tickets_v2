import React from 'react';
import update from 'immutability-helper';

import Utils from '../../utils/utils';
import Api from '../../utils/';
const api = new Api();

export default class Select extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            item:{
                config: [],
                outjson: [],
                filter: []
            }
        });
    }

    componentDidMount(){
        const { name } = this.props;
        $('.ui.dropdown.'+name).dropdown();
        let self = this;
        if(this.props.source === 'letters') {
            const letters = [
                { name: 'А' },{ name: 'Б' },{ name: 'В' },{ name: 'Г' },{ name: 'Д' },{ name: 'Е' },
                { name: 'Ё' },{ name: 'Ж' },{ name: 'З' },{ name: 'И' },{ name: 'К' },{ name: 'Л' },
                { name: 'М' },{ name: 'Н' },{ name: 'О' },{ name: 'П' },{ name: 'Р' },{ name: 'С' },
                { name: 'Т' },{ name: 'У' },{ name: 'Ф' },{ name: 'Х' },{ name: 'Ц' },{ name: 'Ч' },
                { name: 'Ш' },{ name: 'Щ' },{ name: 'Ы' },{ name: 'Э' },{ name: 'Ю' },{ name: 'Я' }
            ];
            self.setState({
                item: {
                    outjson: letters
                }
            });
        } else {
            api.get(this.props.source, {}, {
                s: (res) => {
                    self.setState({
                        item: {
                            config: res.data.config,
                            outjson: res.data.outjson,
                            filter: res.data.outjson
                        }
                    });
                }
            });
        }
        $('.dropdown.'+name).dropdown('set selected', this.props.value);
    }

    componentDidUpdate(){
        const { multi, name } = this.props;
        let self = this;
        $('.dropdown.'+name).dropdown({
            action: 'hide',
            onChange: function(value) {
                if(multi == 'true'){
                    let g = Utils.findIndexByKeyValue(self.state.item.outjson, 'id', value);
                    if( g != null){
                        self.state.item.outjson.map((el,index)=>{
                            if(index == g){
                                self.props.selected(el);
                            }
                        })
                    }
                } else {
                    self.props.selected(value);
                }
            }
        });
    }

    handlerSearch(ev) {
      let value = ev.target.value.toUpperCase();
      let arr = [];
      this.state.item.outjson.map((el)=>{
        if(el.orgname.toUpperCase().indexOf(value) + 1) {
          arr.push(el);
        }
      })
      if(arr.length > 0) {
        this.setState({
          item: update(this.state.item, {
            filter: { $set: arr }
          })
        })
      } else {
        if(value.length > 0) {
          this.setState({
            item: update(this.state.item, {
              filter: { $set: arr }
            })
          })
        } else {
          this.setState({
            item: update(this.state.item, {
              filter: { $set: this.state.item.outjson }
            })
          })
        }
      }
    }

    componentWillReceiveProps(){
      const { name } = this.props;
      $('.ui.dropdown.'+name).dropdown();
    }

    render() {
      // console.log(this.state)
      const { label, name, id, multi } = this.props;
      return (
            <div className='five wide column'>
                <div className={ `ui selection dropdown ${name ? name : ''}` } style={{ width: '100%' }}>
                    <input type='hidden' name='gender' />
                    <i className='dropdown icon'/>
                    <div className='text'>{ label }</div>
                    <div className='menu'>
                        <div className="ui input" style={{ margin: '12px 0px !important'}}>
                            <input type="text" onChange={(ev)=>this.handlerSearch(ev)} placeholder="Поиск..."/>
                        </div>
                        {
                            this.state.item.filter.map(el=>{
                                return <div key={el[id] + 'selitem' + label}
                                            className='item'
                                            data-value={ multi ? el.id : el[id] }>{ el[name] }</div>
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}
