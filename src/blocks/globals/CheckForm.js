import React from 'react';

import Api from '../../utils/';
const api = new Api();

// Чекбокс

export default class Checkbox extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            item:{
                config: [],
                outjson: []
            }
        });
    }

    componentWillMount() {
        this.props.method ? this.props.initItems(this.props.method) : null;
    }

    componentDidMount(){
        const { name } = this.props;
        api.fetchSprav({
            url: this.props.source,
            statePropName: 'item',
            parent: this,
            callback: () => {
                $('#' + name + ' .ui.checkbox').checkbox().on('change', (e) => {
                    this.onRoleChange(e);
                });
            }
        });
    }

    onRoleChange(e) {
        const { value } = this.props;
        let id = + e.target.dataset.id;

        let f = value ? value : [];

        let idx = f.indexOf(id);
        let r = value ? value : [];

        if (idx >= 0) {
            r.splice(idx, 1);
        } else {
            r.push(id);
        }
        this.props.checked(r);
    }

    render() {
        const { value, name, dicid } = this.props;
        return (
            <div className='ui bottom attached segment' id={name}>
                <div className='ui middle aligned divided list'>
                    {
                        this.state.item.outjson.map(i => {
                            let idx = value ? value.indexOf(i.id) : -1;
                            return (
                                <div className='item' key={i.id}>
                                    <div className='left floated content'>
                                        <div className='ui toggle checkbox'>
                                            <input  type='checkbox' name='public'
                                                    checked={(idx != -1)}
                                                    data-id={i[dicid]}
                                                    onChange={el=>{
                                                        el.preventDefault();
                                                    }}
                                            />
                                        </div>
                                    </div>
                                    <div className='content'>{i[name]}</div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}