import React from 'react';
let BASE_URL = 'http://api.jelata.tech/';
/*
  <Search urlS='dic/org?substr='               - api
    name='org'                           - id компонента (optional)
    resultS='outjson'                    - из какого массива брать
    titleS='orgname'                     - title в списке ответов
    defValue={ this.state.item.depname } - дефолтное значение (title)
    descriptionS='shortname'             - описание (доп. инфа) в списке ответов
    styleS={{width: '100%'}}             - опционально: пользовательские стили
    placeHolder=                         - опционально: Placeholder, def: Введите фразу для поиска
    canUndefineInput                    - описание
    callbacks:
      changed={(query) => {
        this.setState({
          item: update(this.state.item, {'depname': {$set: query}})
        })
      }}
      selected={(result) => {
        this.setState({
          item: update(this.state.item, {'depname': {$set: result.depname}})
        })
      }}
      cleared={() => {
        this.setState({
          item: update(this.state.item, {'depname': {$set: ''}})
        })
      }}
  />
*/

let outConfig;

export default class Search extends React.Component {
    constructor(props) {
        super(props);
    }

    goReturn(result) {
        this.props.selected(result, outConfig)
    }

    componentDidMount() {
        let self = this;
        const {urlS, defValue, resultS, titleS, name, cleared, descriptionS} = this.props;

        $.typeahead({
            input: '.js-typeahead-user_v1' + name,
            query: defValue ? defValue : '',
            minLength: 1,
            order: 'asc',
            dynamic: true,
            delay: 500,
            cancelButton: true,
            display: titleS.split(','),
            filter: false,
            emptyTemplate: 'Ничего не нашлось по запросу - {{query}}',
            template: `{{${titleS}}} {{${descriptionS || ''}}}`,
            source: {
                ajax: [{
                    type: 'GET',
                    url: BASE_URL + urlS + '{{query}}',
                    xhrFields: {
                        withCredentials: true
                    },
                    path: resultS,
                    success: function (res) {
                        res.config ? outConfig = (res.config) : outConfig = [];
                    }
                }],
            },
            callback: {
                onClick: function (node, a, item) {
                    //self.goReturn(item);
                    self.props.selected(item,outConfig,true);
                },
                onCancel: function (node, a, item) {
                    cleared ? cleared(item) : null
                }
            },
            debug: true
        });
        $('.js-typeahead-user_v1' + name).trigger('input.typeahead');
    }

    changedVal(e) {
        if (e.keyCode == 13) {
            this.props.changed ? this.props.changed(e.target.value,outConfig,true) : null;
        }
    }

    render() {
        const {disabled, defValue, placeHolder, name} = this.props;
        return (
            <div className='typeahead__container'>
                <div className='typeahead__field'>
                    <span className='typeahead__query'>
                        <input className={ 'js-typeahead-user_v1' + name}
                               name={'js-typeahead-user_v1' + name}
                               type='search'
                               disabled={ disabled ? true : false }
                               defaultValue={ defValue ? defValue : '' }
                               onKeyDown={ ::this.changedVal }
                               placeholder={ placeHolder }
                               autoComplete='off'/>
                    </span>
                </div>

            </div>
        );

    }
}
