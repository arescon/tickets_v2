import React from 'react';
//import { read } from 'fs';

//import Api from '../../utils/';
//const api = new Api();

export default class ListForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editedItemId: null,
            addButtonClicked: false
        }

        this.editFormSubmit = this.editFormSubmit.bind(this);
        this.addFormSubmit = this.addFormSubmit.bind(this);
        this.inputs = {};
    }

    componentWillMount() {
        //this.props.method ? this.props.initItems(this.props.method) : null;
    }

    componentDidMount() {
        $('#' + this.props.id + ' .ui.dropdown').dropdown();
    }

    componentDidUpdate() {
        $('#' + this.props.id + ' .ui.dropdown').dropdown();
        //// console.log('componentDidUpdate', this.state);
    }

    // --- add button
    //render самой кнопки
    addButton() {
        if (this.props.editable && this.props.actions['add'] !== undefined) {
            if (!this.state.addButtonClicked) {
                return (
                    <button className="ui icon button" onClick={i => this.addButtonClick()}>
                        <i className="plus square icon"></i>
                    </button>
                );
            } else {
                return this.editForm({
                    [`${this.props.config.listFieldNames.id}`]: 0,
                    [`${this.props.config.listFieldNames.title}`]: '',
                    [`${this.props.config.listFieldNames.descr}`]: '',
                }, this.addFormSubmit);
            }
        } else return null;
    }

    //add button click handler
    addButtonClick() {
        this.setState({
            addButtonClicked: true
        });
    }

    //отправка данных в container
    addFormSubmit(item) {
        var o = this.getInputsValues(item);
        //// console.log(o);
        this.props.actions['add'](o);
        this.setState({
            addButtonClicked: false,
            editedItemId: null
        });
    }

    // --- edit button
    editButton(item) {
        if (this.props.editable && this.props.actions['edit'] !== undefined) {
            return (
                <div className="item" onClick={i => this.editButtonClick(item)}><i className="pencil alternate icon" title="Редактировать"></i></div>
            );
        } else return null;
    }

    editButtonClick(item) {
        this.setState({
            editedItemId: item[this.props.config.listFieldNames.id]
        });
    }

    editFormSubmit(item) {
        var o = this.getInputsValues(item);
        this.props.actions['edit'](o,this.props.config);
        this.setState({
            editedItemId: null
        });
    }

    //---cancel
    cancelEditClick() {
        this.setState({
            editedItemId: null
        });
    }

    // --- remove button
    removeButton(item) {
        if (this.props.actions['remove'] !== undefined) {
            return (
                <div className="item" onClick={i => this.removeButtonClick(item)}><i className="trash alternate icon" title="Удалить"></i></div>
            );
        } else return null;
    }

    removeButtonClick(item) {
        if (window.confirm(`Удалить ${item[this.props.config.listFieldNames.title]} ?`)) {
            this.props.actions['remove'](item, this.props.config);
        }
    }

    //---input
    prepareInputs(item) {
        let keys = Object.keys(item);
        keys.map(fieldName => {
            switch (this.props.config.allFields[fieldName].type) {
                case 'int':
                    try {
                        item[fieldName] = parseInt(item[fieldName]);
                        if (isNaN(item[fieldName]))
                            item[fieldName] = null;
                    } catch (e) {
                        item[fieldName] = null;
                    }
                    break;
                case 'numeric':
                    try {
                        item[fieldName] = parseFloat(item[fieldName]);
                        if (isNaN(item[fieldName]))
                            item[fieldName] = null;
                    } catch (e) {
                        item[fieldName] = null;
                    }
                    break;
                default:
                    break;
            }
        });

        return item;
    }

    getInputsValues(item) {
        var o = { ...item };

        //// console.log('getInputsValues', o);

        Object.keys(this.inputs).map(i => {
            o[i] = this.inputs[i].value;
        });

        o = this.prepareInputs(o);
        //// console.log('preparedInputs', o);

        return o;
    }

    inputElement(data, fieldName, fieldConfig) {
        const fieldProps = Object.assign({}, fieldConfig.props);
        //// console.log('data', data);
        switch (fieldConfig.type) {
            case 'string':
                return (
                    <input type="text"
                        readOnly={(fieldConfig.readonly === true)}
                        required={(fieldConfig.required === true)}
                        minLength={fieldProps.min}
                        maxLength={fieldProps.max}
                        defaultValue={data[fieldName]}
                        ref={(input) => this.inputs[fieldName] = input}
                        placeholder="Введите значение..." />
                );
                break;
            case 'int':
                if (isNaN(data[fieldName]))
                    data[fieldName] = '';
                return (
                    <input type="number"
                        readOnly={(fieldConfig.readonly === true)}
                        required={(fieldConfig.required === true)}
                        min={fieldProps.min} max={fieldProps.max}
                        defaultValue={data[fieldName]}
                        ref={(input) => this.inputs[fieldName] = input}
                        placeholder="Введите значение..." />
                );
                break;
            case 'numeric':
                if (isNaN(data[fieldName]))
                    data[fieldName] = '';
                return (
                    <input type="number"
                        readOnly={(fieldConfig.readonly === true)}
                        required={(fieldConfig.required === true)}
                        step={fieldProps.step}
                        min={fieldProps.min}
                        max={fieldProps.max}
                        defaultValue={data[fieldName]}
                        ref={(input) => this.inputs[fieldName] = input}
                        placeholder="Введите значение..." />
                );
                break;
            default:
                return null;
                break;
        }

    }

    //editForm
    editForm(dataItem, submitFunc) {
        this.inputs = {};

        const editFieldNames = [];
        Object.keys(this.props.config.allFields).map(i => {
            if (this.props.config.allFields[i].editable === true)
                editFieldNames.push(i);
        });

        return (
            <form className="ui form">
                {
                    editFieldNames.map((fieldName, idx) => {
                        let fieldConfig = this.props.config.allFields[fieldName];
                        return (
                            <div className="field" key={idx}>
                                <label>{fieldConfig.langs[this.props.userLang]}</label>
                                {this.inputElement(dataItem, fieldName, fieldConfig)}
                            </div>
                        )
                    })
                }
                <button className="ui icon green basic button" type="button" onClick={i => { submitFunc(dataItem) }} title="Сохранить">
                    <i className="save icon"></i>
                </button>
                <button className="ui icon red basic button" type="button" onClick={i => { this.cancelEditClick() }} title="Отмена редактирования">
                    <i className="window ban icon"></i>
                </button>
            </form>
        )
    }

    //render
    render() {
        const { dataItems, config, editable, actions, listClasses, title } = this.props;
        const actionAdd = (actions['add'] !== undefined ? actions.add : null);
        const actionClick = (this.props.actions['click'] ? this.props.actions.click : null);

        //const actionEdit = (actions['edit'] !== undefined ? actions.edit : null);
        //const actionRemove = (actions['remove'] !== undefined ? actions.remove : null);
        return (
            <div id={this.props.id}>
                <h3>{title ? title : ''}</h3>

                {this.addButton()}

                <div className={`ui list ${listClasses}`}>
                    {
                        dataItems.map((item, idx) => {
                            let isCurrentItemIsEdited = item[config.listFieldNames.id] === this.state.editedItemId;
                            let contentWidth = this.props.editable === true ? 'thirteen' : 'fourteen';
                            //let contentWidth = isCurrentItemIsEdited === true ? 'thirteen' : 'fourteen';

                            return (
                                <div className="item hover" key={idx}>
                                    {
                                        (isCurrentItemIsEdited === false) ?
                                            <div className="ui grid">
                                                <div className="two wide column">
                                                    <div className="ui top aligned horizontal label">{item[config.listFieldNames.id]}</div>
                                                </div>
                                                <div className={`${contentWidth} wide column`}>
                                                    {
                                                        <div className="middle aligned content">
                                                            <a className="header" onClick={i => actionClick(item)}>{item[this.props.config.listFieldNames.title]}</a>
                                                            <div className="description">{item[this.props.config.listFieldNames.descr]}</div>
                                                        </div>
                                                    }
                                                </div>
                                                {
                                                    (this.props.editable === true) ?
                                                        <div style={{
                                                            position: 'absolute',
                                                            right: '0',
                                                            marginTop: '15px'
                                                        }}>
                                                            <div className="ui right pointing dropdown icon small button">
                                                                <i className="wrench icon"></i>
                                                                <div className="menu">
                                                                    {this.editButton(item)}
                                                                    {this.removeButton(item)}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        :
                                                        null
                                                }
                                            </div>
                                            :
                                            <div className="ui segment">
                                                <div className="ui top attached middle aligned label">{item[config.listFieldNames.id]}</div>
                                                {
                                                    this.editForm(item, this.editFormSubmit)
                                                }
                                            </div>
                                    }
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        )
    }
}