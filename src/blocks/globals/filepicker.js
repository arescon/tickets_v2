import React from 'react';
import Utils from '../../utils/utils';
import Api from '../../utils/';
const api = new Api();
const prefix = 'api';

/**
 * Загрузчик файлов
 * 
 * @param {array} files Массив файлов
 * 
 */
export default class FilePickerBlock extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {icon, buttonStyle, label, showFiles} = this.props;
        return [
            <div className='field' key='fpb22'>
                <button className={ buttonStyle ? buttonStyle : 'ui button blue' }
                        onClick={()=>{document.getElementById('filePicker').click();}}>
                            { icon ? <i className={`${icon} icon`}/> : null }
                        { label ? label : null } </button>
                <input  type='file'
                        id='filePicker'
                        hidden='hidden'
                        multiple={true}
                        // onLoadStart={()=>console.log('start')}
                        onChange={e=>this.props.change(e.target.files)}/>
            </div>,
            <div className='ui divided items' key='fpii'>
                <FilesRender {...this.props}  />
                {
                  (Array.isArray(showFiles) && showFiles.length > 0) ?
                    showFiles.map((el, i)=>{
                      return (
                        <div key={`${i}filerendd`} className='item'>
                          <div className='ui image'>
                            <i className='file alternate icon'/>
                          </div>
                          <div className='middle aligned content'>
                            <a target='_blank' href={el.filepath}>
                              {el.filename}
                            </a>
                          </div>
                        </div>
                      )
                    }) : null
                }
            </div>
        ]
    }
}

const FilesRender = (props) => {
  let { files, showFiles } = props;
  if(Array.isArray(files) && files.length > 0) 
      return files.map((el, i)=>{
          if(el.type === 'image/jpeg' || el.type === 'image/png')
              return (
                  <div key={`${i}filerend`} className='item'>
                      <div className='ui tiny image'>
                          <ImageRender src={el} />
                      </div>
                      <div className='middle aligned content'>
                          <h4 className='ui header'>
                              { el.name }
                              <div className='ui sub header'>
                                  { (el.size/1048576).toFixed(2) } mb
                              </div>
                          </h4>
                          <button style={{ float: 'right'}}
                                  data-id={i}
                                  onClick={ev=>props.handleDelete(ev)}
                                  className='ui red basic button'>
                              Удалить
                          </button>
                      </div>
                  </div>
              )
          else return (
            <div key={`${i}filerendd`} className='item'>
              <div className='ui tiny image'>
                <i className='file alternate icon'/>
              </div>
              <div className='middle aligned content'>
                <h4 className='ui header'>
                    { el.name }
                    <div className='ui sub header'>
                        { (el.size/1048576).toFixed(2) } mb
                    </div>
                  </h4>
                  <button style={{ float: 'right'}}
                          data-id={i}
                          onClick={ev=>props.handleDelete(ev)}
                          className='ui red basic button'>
                      Удалить
                  </button>
              </div>
            </div>
          )
      })
    else return null
}

class ImageRender extends React.Component {
    constructor(props) {
        super(props);
        this.state=({
            url: ''
        })
    }
    componentWillMount() {
        Utils.getBase64(this.props.src, (res) => {
            this.setState({
                url: res
            })
        })
    }
    render() {
        return <img key='dawer9' src={this.state.url} />
    }
}
