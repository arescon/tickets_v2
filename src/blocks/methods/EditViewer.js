// <EditViewer item={ t }                               - данные
//             config={ this.state.arrBBK.config }      - конфиг из БД
//             onSave={ ::this.SaveItem } />,           - callback (выводит наружу текущее состояние редактируемого объекта)

import React from 'react';
import update from 'immutability-helper';
import ApiFactory from './../../utils/index';
import Checkbox from './../globals/CheckForm';

const api = new ApiFactory();

export default class EditViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item,
            roles: {
                config: {},
                outjson: []
            }
        };
        this.id = 'methodOne';
        this.props.initItems(this.props.method+'?id='+this.props.id);
    }

    componentDidMount(){
        api.fetchSprav({
            url: 'api/roles',
            statePropName: 'roles',
            parent: this,
            callback: () => {
                $('#' + this.id + ' .ui.checkbox').checkbox().on('change', (e) => {
                    this.onRoleChange(e);
                });
            }
        });
    }

    onRoleChange(e) {
        let id = + e.target.dataset.id;

        let idx = this.state.item.roles.indexOf(id);
        let r = this.state.item.roles;

        if (idx >= 0) {
            r.splice(idx, 1);
        } else {
            r.push(id);
        }

        this.setState({
            item: update(this.state.item, {roles: {$set: r}})
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({item: nextProps.item})
    }

    changeValue(e) {
        let mode = e.currentTarget.dataset.mode;
        let type = e.currentTarget.dataset.type;
        let s = e.target.value;
        this.setState({
            item: update(this.state.item,
                {   config: {
                        [`${type}`]: {
                            [`${mode}`]: {$set: s}
                        }
                    }
                }
            )
        })
    }

    changeName(e) {
        let mode = e.currentTarget.dataset.mode;
        let s = e.target.value;

        this.setState({
            item: update(this.state.item,
                { [`${mode}`]: {$set: s} }
            )
        })
    }

    saveChanges(e) {
        e.preventDefault();
        this.props.saveItem(this.state.item, this.props.method);
    }

    render() {
        let t = this.state.item ? this.state.item.config ? Object.keys(this.state.item.config) : [] : [];

        if(this.state.item) {
            return (
                <div className='ui form' id='methodOne'>
                    <div className='inline field'>
                        <label>ID: </label>
                        { this.state.item.id }
                    </div>
                    <div className='field'>
                        <label>Название функции (обязательно указывать с именем схемы, иначе не будет работать)</label>
                        <input  value={ this.state.item.fnname }
                                data-mode='fnname'
                                onChange={ (e)=>this.changeName(e) }/>
                    </div>
                    <div className='field'>
                        <label>Название метода</label>
                        <input  value={ this.state.item.methodname ? this.state.item.methodname : '' }
                                data-mode='apiname'
                                onChange={ (e)=>this.changeName(e) }/>
                    </div>

                    <div className='field' style={{borderBottom: '1px solid rgb(222, 222, 222)'}}>
                        Роли
                    </div>
                    <div className='inline fields'>
                        <Checkbox label=''
                            source='api/roles'
                            name='rolename'
                            dicid='id'
                            value={this.state.item.roles ? this.state.item.roles : []}
                            checked={(result) => {
                                this.setState({
                                    item: update(this.state.item,
                                        {
                                            ['roles']: { $set: result }
                                        }
                                    )
                                })
                            }}
                        />
                    </div>

                    <div className='field' style={{borderBottom: '1px solid rgb(222, 222, 222)'}}>
                        Конфиг
                    </div>
                    <div className='inline fields'>
                        <div className='six wide field'>
                            <label>Назв</label>
                        </div>
                        <div className='five wide field'>
                            <label>Title</label>
                        </div>
                        <div className='five wide field'>
                            <label>Type</label>
                        </div>
                        <div className='five wide field'>
                            <label>isTitle</label>
                        </div>
                        <div className='five wide field'>
                            <label>DicAPI</label>
                        </div>
                        <div className='five wide field'>
                            <label>DicTitle</label>
                        </div>
                        <div className='five wide field'>
                            <label>DicID</label>
                        </div>
                    </div>
                    { t.map(item => {
                        return (
                            <div className='inline fields' key={ item } style={{ paddingBottom: '2px', borderBottom: '1px solid #eee', marginBottom: '2px'}}>
                                <div className='six wide field'>
                                    { item }
                                </div>
                                <div className='five wide field'>
                                    <input  value={ this.state.item.config[item]['title'] }
                                            data-type={ item }
                                            data-mode='title'
                                            onChange={ (e)=>this.changeValue(e) }/>
                                </div>
                                <div className='five wide field'>
                                    <input  value={ this.state.item.config[item]['type'] }
                                            data-type={ item }
                                            data-mode='type'
                                            onChange={ (e)=>this.changeValue(e) }/>
                                </div>

                                <div className='five wide field'>
                                    <input  value={ this.state.item.config[item]['istitle'] }
                                            data-type={ item }
                                            data-mode='istitle'
                                            onChange={ (e)=>this.changeValue(e) }/>
                                </div>

                                <div className='five wide field'>
                                    <input  value={ this.state.item.config[item]['dicapi'] }
                                            data-type={ item }
                                            data-mode='dicapi'
                                            onChange={ (e)=>this.changeValue(e) }/>
                                </div>
                                <div className='five wide field'>
                                    <input  value={ this.state.item.config[item]['dictitle'] }
                                            data-type={ item }
                                            data-mode='dictitle'
                                            onChange={ (e)=>this.changeValue(e) }/>
                                </div>
                                <div className='five wide field'>
                                    <input  value={ this.state.item.config[item]['dicid'] }
                                            data-type={ item }
                                            data-mode='dicid'
                                            onChange={ (e)=>this.changeValue(e) }/>
                                </div>
                            </div>
                        )})
                    }
                    <div className='field'>
                        <button className='ui positive button'
                                onClick={ (e)=>this.saveChanges(e) }>Сохранить
                        </button>
                    </div>
                </div>
            )  
        } else {
            return null;
        }
        
    }
}