import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import * as authAction from '../../redux/actions/auth';

const api = new Api();

class Notys extends Component {
  constructor(props) {
    super(props);
  }
  handlerCloseNoty(id) {
    api.get(`api/setsended?id=${id}`);
  }
  handlerClear() {
    api.get(`api/setsendedall`);
  }
  note_render() {
    const { auth } = this.props;
    if(Array.isArray(auth.notys) && auth.notys.length > 0) {
      return auth.notys.map((el,i)=>{
        return (
          <div key={`${i}-notys`} className='item'>
            <i className='large envelope outline middle aligned icon'></i>
            <div className='content'>
              <Link
                to={`/dashboard/tikets/ticket/${el.tableid}`}
                onClick={()=>this.handlerCloseNoty(el.id)}
                className='header'
              >{el.tableid}</Link>
              <div style={{
                position: 'absolute',
                right: '0',
                color: '#b1b1b1',
                fontSize: '0.8em',
                marginTop: '-20px'
              }}>{dateFormater(el.created, true)}</div>
              <div className='description'>{el.title}</div>
            </div>
          </div>
        )
      })
    } else {
      return (
        <div>
          Уведомлений нет
        </div>
      )
    }
  }
  render() {
    const { auth } = this.props;
    return [
      <button key='but_ws_clr' className='ui button blue basic' onClick={()=>this.handlerClear()}>Отметить все прочитанными</button>,
        <div key='blc_ws' className='ui relaxed divided list' visible={Array.isArray(auth.notys) && auth.notys.length > 0 ? true : false }>
        {this.note_render()}
      </div>
    ]
  }
}

Notys.propTypes = {
  auth: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Notys);

/**
 * Преобразование даты в вид 21 февраля 2018;
 * @param date дата в формате 2018-02-27T14:43:51.352953
 * @param watch (true, false) показать время или нет
 * 
 * @returns дата в формате 27 февраля 2018 время (14:26)
 */
function dateFormater(date, watch) {
  let c = new Date(date);
  let d = [];
  var o = {};
  let dc = new Date();
  if(watch){
      o = {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          timezone: 'UTC',
          hour: 'numeric',
          minute: 'numeric'
      };
      d.y = c.getFullYear();
      d.m = c.getMonth();
      d.d = c.getDate();
      d.h = c.getHours();
      d.min = c.getMinutes();
      dc = new Date(d.y, d.m, d.d, d.h, d.min);
  } else {
      o = {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
      };
      d.y = c.getFullYear();
      d.m = c.getMonth();
      d.d = c.getDate();
      let dc = new Date(d.y, d.m, d.d);
  }

  return dc.toLocaleString('ru', o);
}