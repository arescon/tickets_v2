import React, {Component} from 'react';
import {connect} from 'react-redux';
import update from 'immutability-helper';
// import TinyMCE from 'react-tinymce';
import Search from '../globals/Search';
import Select from '../globals/selectForm';
import Utils from '../../utils/utils';
import Checkbox from '../globals/CheckForm';

import Api from '../../utils/';
const api = new Api();

let photo;

function file_picker_callback(callback, value, meta) {
    if (meta.filetype === 'image') {
        $('#uploadFile').trigger('click');
        $('#uploadFile').on('change', function() {
            let file = this.files[0];
            let reader = new FileReader();
            reader.onload = function(e) {
                callback(e.target.result, {
                    alt: ''
                });
            };
            reader.readAsDataURL(file);
        });
    }
}

class EditorBlock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.items,
            choosed: false,

            fileName: 'Выберите файл',
            fileSize: '',
            file: null,
            description: '',
            isClose: false,
            fileURL: null,

            files: [{
                photo: null,
                fileName: 'Выберите файл',
                fileSize: '',
                file: null,
                description: '',
                isClose: false,
                fileURL: null
            }],

            roles: {
                config: [],
                outjson: []
            }
        };
        this.props.initItems(this.props.method+'?id='+this.props.id);
        this.filesRender = this.filesRender.bind(this);
    }

    save(e){
        e.preventDefault();
        this.props.saveItem(this.state.item, this.props.method);
    }

    update(e){
        e.preventDefault();
        this.props.updateItem(this.state.item, this.props.method);
    }

    change(e) {
        this.setState({
            item: update(this.state.item,
                {
                    [`${e.currentTarget.dataset.name}`]: {$set: e.currentTarget.value}
                }
            )
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({item: nextProps.items})
    }

    getMultiParams(conf) {
        let area;
        this.state.item ? area = this.state.item[conf.dicid] : area = [];
        return (
            area ? area.map(k => {
                    return (
                        <div className='ui image label' key={
                            conf.dictitle.split(',').map(it =>{
                                return k[it];
                            })
                        }>
                            { conf.dictitle.split(',').map(it =>{
                                return k[it] + ' ';
                            }) }
                            <i className='delete icon'
                               data-id={ k.id }
                               data-dicid={ conf.dicid }
                               onClick={::this.removeMultiElement }/>
                        </div>
                    )
                }
            ) : null
        )

    }

    getMultiParamsSelect(conf) {
        let area;
        this.state.item ? area = this.state.item[conf.dicid] : area = [];
        return (
            area ? area.map(k => {
                    return (
                        <div className='ui image label' key={ conf.dictitle.split(',').map(it =>{
                            return k[it];
                        }) }>
                            { conf.dictitle.split(',').map(it =>{
                                return k[it] + ' ';
                            }) }
                            <i className='delete icon'
                               data-id={ k.id }
                               data-dicid={ conf.dicid }
                               onClick={::this.removeMultiElement }/>
                        </div>
                    )
                }
            ) : null
        )

    }

    removeMultiElement(e) {
        e.preventDefault();
        let id = e.currentTarget.dataset.id;
        let dicid = e.currentTarget.dataset.dicid;
        let area;
        area = $.grep(this.state.item[dicid], (k) => {
            return +k.id == +id;
        })[0];

        let g = this.state.item[dicid] ? this.state.item[dicid] : [];
        g.splice(Utils.findIndexByObj(g, area), 1);
        this.setState({
            item: update(this.state.item,
                {
                    [`${dicid}`]: {$set: g}
                }
            )
        });
    }

    changeObj(e){
        //e.preventDefault();
        let id = e.currentTarget.dataset.name;
        let arr = JSON.parse(e.currentTarget.dataset.dicapi);
        let elem = e.currentTarget.dataset.elem;
        let val = e.currentTarget.value;
        if(this.state.item[id]){
            this.setState({
                item: update(this.state.item,
                    {
                        [`${id}`]: {
                            [`${elem}`]: { $set: val }
                        }
                    }
                )
            })
        } else {
            arr[elem] = val;
            this.setState({
                item: update(this.state.item,
                    {
                        [`${id}`]: { $set: arr }
                    }
                )
            })
        }
    }

    changeUserPic(event) {
        // event.preventDefault();
        let reader = new FileReader();
        reader.onloadend = function () {
            photo = reader.result;
        };

        let t = Math.round(event.currentTarget.files[0].size / (1024)) + ' Kb';


        this.setState({
            item: update(this.state.item,
                {
                    file: { $set: photo }
                }
            ),
            choosed: true,
            fileName: event.currentTarget.files[0].name,
            file: event.currentTarget.files[0],
            fileSize: t,
            fileURL: URL.createObjectURL(event.currentTarget.files[0])
        }, () => {
            $('.ui.checkbox').checkbox()
        });

    }

    changeFilesPic(event) {
        let photo2;
        let index = event.currentTarget.dataset.id;
        let reader = new FileReader();
        reader.onloadend = function () { photo2 = reader.result };
        let t = Math.round(event.currentTarget.files[0].size / (1024)) + ' Kb';
        let arr = {
            photo: photo2,
            choosed: true ,
            fileName: event.currentTarget.files[0].name,
            file: event.currentTarget.files[0],
            fileSize: t,
            fileURL: URL.createObjectURL(event.currentTarget.files[0])
        };
        this.setState({
            files: update(this.state.files,
                {
                    [`${index}`]: { $set: arr}
                }
            )
        });

    }

    AddFile() {
        let indexLast = this.state.files.length;
        this.setState({
            files: update(this.state.files,
                {
                    [`${indexLast}`]: { $set: {
                            photo: null,
                            fileName: 'Выберите файл',
                            fileSize: '',
                            file: null,
                            description: '',
                            isClose: false,
                            fileURL: null
                        }}
                }
            )
        });
    }

    onRoleChange(e) {
        let id = + e.target.dataset.id;

        let idx = this.state.item.roles.indexOf(id);
        let r = this.state.item.roles;

        if (idx >= 0) {
            r.splice(idx, 1);
        } else {
            r.push(id);
        }

        this.setState({
            item: update(this.state.item, {roles: {$set: r}})
        })
    }

    changeBackGroundClick(event) {
        event.preventDefault();
        let index = event.currentTarget.dataset.id ? event.currentTarget.dataset.id : '';
        $(`#hidBckInput${index}`).click();
    }

    componentDidMount() {
        $('select.dropdown').dropdown();
        $('input.time').mask('00:00:00');

        // api.fetchSprav({
        //     url: 'wbp/roles',
        //     statePropName: 'roles',
        //     parent: this,
        //     callback: () => {
        //         $('#' + this.id + ' .ui.checkbox').checkbox().on('change', (e) => {
        //             this.onRoleChange(e);
        //         });
        //     }
        // });
    }

    componentDidUpdate() {
        $('select.dropdown').dropdown();
        $('input.time').mask('00:00:00');
    }

    filesRender() {
        let self = this;
        return self.state.files.map((el,i)=>{
            return (
                <div key={i} className='ten wide ui grid column' style={{marginTop: '0em'}}>
                    <div className='nine wide column' style={{height: '150px'}}>
                        <img    className='ui small image'
                                style={{ height: '120px', width: 'auto' }}
                                id='imagess'
                            //src={ (this.state.fileURL !== null) ? this.state.fileURL : 'http://'+this.state.item.photo }/>
                                src={ (el.fileURL !== null) ? el.fileURL : '' }/>
                    </div>
                    <div className='six wide column'>
                        <a  className='ui blue button fluid'
                            data-id={i}
                            onClick={::self.changeBackGroundClick}>{`${el.fileName}, ${el.fileSize} `}</a>
                        <input  type='file'
                                data-id={i}
                                onChange={::self.changeFilesPic}
                                hidden='hidden'
                                id={`hidBckInput${i}`}/>
                    </div>
                </div>
            );
        })
    }

    handleBack() {
        this.props.goBack(this.props.currentLocation);
    }

    render() {
        const { id, user, config} = this.props;
        let t = config ? Object.keys(config) : [];
        let self = this;

        // console.log(this.state);

        return (
            <div>
                <div className='ui segment'>
                    { (this.props.goBack) ?
                        <button className='ui button' onClick={::this.handleBack}>Назад</button> :
                        null }
                    { (id == 0) ?
                        <button className='ui button green' onClick={::this.save}>Добавить</button> :
                        <button className='ui button green' onClick={::this.update}>Сохранить</button>}
                </div>
                <div className='ui segment'>
                    <input name='image' type='file' id='uploadFile' className='hidden' style={{ display: 'none' }}/>
                    <form className='ui form grid'>
                        { t.map((item) => {
                            switch (config[item].type) {
                                case 'hidden':
                                    return <input key={config[item].type + item} 
                                                  type='hidden'
                                                  defaultValue={ this.state.item ? this.state.item[item] : '' } />;
                                case 'text':
                                    return (
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <input type='text'
                                                   data-name={item}
                                                   value={ this.state.item ? this.state.item[item] : '' }
                                                   onChange={::this.change}
                                                   placeholder={ config[item].title }/>
                                        </div>
                                    );
                                case 'number':
                                    return (
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <input type='number'
                                                   data-name={item}
                                                   value={ this.state.item ? this.state.item[item] : '' }
                                                   onChange={::this.change}
                                                   placeholder={ config[item].title }/>
                                        </div>
                                    );
                                case 'date':
                                    return (
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <input type='date'
                                                   data-name={item}
                                                   value={ this.state.item ? this.state.item[item] : '' }
                                                   onChange={::this.change}
                                                   placeholder={ config[item].title }/>
                                        </div>
                                    );
                                case 'time':
                                    return (
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <input type='text'
                                                   className='time'
                                                   data-name={item}
                                                   value={ this.state.item ? this.state.item[item] : '' }
                                                   maxLength='8'
                                                   onChange={::this.change}/>
                                        </div>
                                    );
                                case 'search':
                                    return (
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <Search urlS={ config[item].dicapi }
                                                    resultS='outjson'
                                                    titleS={ config[item].dictitle }
                                                    descriptionS={ config[item].dictitle }
                                                    styleS={{width: '100%'}}
                                                    name={ config[item].type + item + item }
                                                    defValue={ this.state.item ? this.state.item[config[item].istitle] : '' }
                                                    placeHolder={ this.state.item ? this.state.item[config[item].istitle] : config[config[item].istitle] }
                                                    selected={(result) => {
                                                        this.setState({
                                                            item: update(this.state.item,
                                                                {
                                                                    [`${item}`]: {$set: result[config[item].dicid]}
                                                                }
                                                            )
                                                        })
                                                    }}
                                                    cleared={() => {
                                                        this.setState({
                                                            item: update(this.state.item,
                                                                {
                                                                    [`${item}`]: {$set: ''}
                                                                }
                                                            )
                                                        })
                                                    }}
                                            />
                                        </div>
                                    );
                                case 'multipart':
                                    return (
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label >{ config[item].title }
                                                <i className='ui icon list layout right floated'/>
                                            </label>
                                            <Search urlS={ config[item].dicapi }
                                                    resultS='outjson'
                                                    titleS={ config[item].dictitle }
                                                    descriptionS={ config[item].dictitle }
                                                    styleS={{width: '100%'}}
                                                    multi='true'
                                                    name={config[item].type + item + item}
                                                    selected={ (result) => {
                                                        let g = this.state.item[config[item].dicid] ? this.state.item[config[item].dicid] : [];
                                                        if(Utils.findIndexByKeyValue(g, 'id', result.id) == null){
                                                            g.push(result);
                                                            this.setState({
                                                                item: update(this.state.item,
                                                                    {
                                                                        [`${config[item].dicid}`]: {$set: g}
                                                                    }
                                                                )
                                                            })
                                                        }
                                                    }}
                                            />
                                            { this.getMultiParams(config[item]) }
                                        </div>
                                    );
                                case 'select':
                                    return(
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <Select     label={ this.state.item[config[item].istitle] }
                                                        source={ config[item].dicapi }
                                                        name={ config[item].dictitle }
                                                        id={ config[item].dicid }
                                                        value={ this.state.item ? this.state.item[config[item].istitle] : '' }
                                                        selected={ (result) => {
                                                            this.setState({
                                                                item: update(this.state.item,
                                                                    {
                                                                        [`${item}`]: {$set: result}
                                                                    }
                                                                )
                                                            })
                                                        }}
                                            />
                                        </div>
                                    );
                                case 'multiselect':
                                    return(
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <Select     label={ config[item] }
                                                        source={ config[item].dicapi }
                                                        user={user}
                                                        multi='true'
                                                        name={ config[item].dictitle }
                                                        id={ config[item].dictitle }
                                                        value={ '' }
                                                        selected={ (result) => {
                                                            let g = this.state.item[config[item].dicid] ? this.state.item[config[item].dicid] : [];
                                                            if(Utils.findIndexByKeyValue(g, 'id', result) == null){
                                                                g.push(result);
                                                                this.setState({
                                                                    item: update(this.state.item,
                                                                        {
                                                                            [`${config[item].dicid}`]: {$set: g}
                                                                        }
                                                                    )
                                                                })
                                                            }
                                                        }}
                                            />
                                            { this.getMultiParamsSelect(config[item]) }
                                        </div>
                                    );

                            }
                        }) }
                        { t.map((item) => {
                            switch (config[item].type) {
                                case 'textarea':
                                    return(
                                        <div key={config[item].type + item}
                                             className='sixteen wide column'>
                                            <label>{ config[item].title }</label>
                                            <textarea   data-name={item}
                                                        value={ this.state.item ? this.state.item[item] : '' }
                                                        onChange={::this.change}
                                                        placeholder={ config[item].title }/>
                                        </div>
                                    );
                            }
                        }) }
                        { t.map((item) => {
                            switch (config[item].type) {
                                case 'checkbox':
                                    return(
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{ config[item].title }</label>
                                            <Checkbox   label={ config[item] }
                                                        source={ config[item].dicapi }
                                                        name={ config[item].dictitle }
                                                        dicid={ config[item].dicid }
                                                        value={ this.state.item ? this.state.item[item] : [] }
                                                        checked={ (result) => {
                                                            this.setState({
                                                                item: update(this.state.item,
                                                                    {
                                                                        [`${item}`]: {$set: result}
                                                                    }
                                                                )
                                                            })
                                                        }}
                                            />
                                        </div>
                                    );
                                case 'obj': {
                                    let obj = JSON.parse(config[item].dicapi);
                                    let headd = Object.keys(obj);
                                    let value;
                                    return (
                                        <div key={config[item].type + item}
                                             className='five wide column'>
                                            <label>{config[item].title}</label>
                                            <div className='inline field'>
                                                {headd.map(elem => {
                                                    value = '';
                                                    this.state.item ? (
                                                        this.state.item[item] ? value = this.state.item[item][elem] : value = ''
                                                    ) : value = '';
                                                    return (
                                                        <div className='sixteen wide field' key={elem + elem}>
                                                            <label>{elem}</label>
                                                            <input type='text'
                                                                   data-name={item}
                                                                   data-dicapi={config[item].dicapi}
                                                                   data-elem={elem}
                                                                   value={value}
                                                                   onChange={::this.changeObj}
                                                                   placeholder={elem}/>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    );
                                }
                            }
                        }) }
                        { /*t.map((item) => {
                            switch (config[item].type) {
                                case 'editor': {
                                    let obj = JSON.parse(config[item].dicapi);
                                    let headd = Object.keys(obj);
                                    let value;
                                    return (
                                        <div key={config[item].type + item}
                                             className='sixteen wide column'>
                                            <label>{config[item].title}</label>

                                            <div className='field'>
                                                {headd.map(elem => {
                                                    value = '';
                                                    this.state.item ? (
                                                        this.state.item[item] ? value = this.state.item[item][elem] : value = ''
                                                    ) : value = '';
                                                    return (
                                                        <div key={elem+elem+'aass'}>
                                                            <TinyMCE
                                                                content={ value }
                                                                config={{
                                                                    plugins: 'link image contextmenu table code textcolor colorpicker directionality',
                                                                    language: 'ru',
                                                                    contextmenu: 'link image | inserttable | cell row column deletetable',
                                                                    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
                                                                    image_advtab: true,
                                                                    toolbar: 'undo redo | sizeselect | bold italic | fontselect | fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify | code',
                                                                    file_picker_callback: file_picker_callback
                                                                }}
                                                                onChange={(e) => {
                                                                    let id = item;
                                                                    let arr = JSON.parse(config[item].dicapi);
                                                                    let elem2 = elem;
                                                                    let val = e.target.getContent();

                                                                    if(self.state.item[id]){
                                                                        this.setState({
                                                                            item: update(self.state.item,
                                                                                {
                                                                                    [`${id}`]: {
                                                                                        [`${elem2}`]: { $set: val }
                                                                                    }
                                                                                }
                                                                            )
                                                                        })
                                                                    } else {
                                                                        arr[elem2] = val;
                                                                        self.setState({
                                                                            item: update(self.state.item,
                                                                                {
                                                                                    [`${id}`]: {$set: arr}
                                                                                }
                                                                            )
                                                                        })
                                                                    }
                                                                }}
                                                            />
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    );
                                }
                            }
                        }) */}
                        { t.map((item) => {
                            switch (config[item].type) {
                                case 'file':
                                    return (
                                        <div key={config[item].type + item} className='sixteen wide column'>
                                            <h4 className='ui dividing header'>{ config[item].title }</h4>
                                            <div className='ui grid'>
                                                <div className='five wide column'>
                                                    <img className='ui small image'
                                                         id='imagess'
                                                        //src={ (this.state.fileURL !== null) ? this.state.fileURL : 'http://'+this.state.item.photo }/>
                                                         src={ (this.state.fileURL !== null) ? this.state.fileURL : this.state.item.photo }/>
                                                </div>
                                                <div className='five wide column'>
                                                    <a className='ui blue button fluid'
                                                       onClick={::this.changeBackGroundClick}>{`${this.state.fileName}, ${this.state.fileSize} `}</a>
                                                    <input type='file' onChange={::this.changeUserPic} hidden='hidden' id='hidBckInput'/>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                case 'img':
                                    return (
                                        <div key={config[item].type + item} className='sixteen wide column'>
                                            <h4 className='ui dividing header'>{ config[item].title }</h4>
                                            <div className='ui grid'>
                                                <div className='five wide column'>
                                                    <img className='ui small image'
                                                         id='imagess'
                                                        //src={ (this.state.fileURL !== null) ? this.state.fileURL : 'http://'+this.state.item.photo }/>
                                                         src={ this.state['item'][config[item].istitle] ? this.state['item'][config[item].istitle] : config[item].dicapi }/>
                                                </div>
                                                <div className='five wide column'>
                                                    <a className='ui blue button fluid'
                                                       onClick={::this.changeBackGroundClick}>{`${this.state.fileName}, ${this.state.fileSize} `}</a>
                                                    <input type='file' onChange={::this.changeUserPic} hidden='hidden' id='hidBckInput'/>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                case 'files':
                                    return (
                                        <div key={config[item].type + item} className='sixteen wide column'>
                                            <h4 className='ui dividing header'>{ config[item].title } (multifiles)</h4>
                                            <a className='ui green button' style={{ position: 'absolute', marginTop: '-44px', marginLeft: '185px', padding: '7px 12px' }} onClick={ ::this.AddFile }>+</a>
                                            <div className='ui grid'>
                                                { this.filesRender() }
                                            </div>
                                        </div>
                                    )
                            }
                        }) }
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}
export default connect(mapStateToProps)(EditorBlock)
