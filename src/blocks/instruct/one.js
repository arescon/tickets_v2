import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import FilePickerBlock from '../globals/filepicker';
import Select from './../../box/select';

import * as authAction from '../../redux/actions/auth';

const api = new Api();
const prefix = 'api';

class Istr_one extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      item: {},
      files: {
        items: []
      }
    });
    let self = this;
    api.get(`${prefix}/instruction?id=${ this.props.id }`, {}, {
      s: (res) => {
        self.setState({
          item: res.data.outjson
        });
      }
    });
  }

  handleChange(e) {
    let type = e.target.dataset.type;

    this.setState({
      item: update(this.state.item, {
        [`${type}`]: { $set: e.target.value }
      })
    })
  }
  handleFiles(e) {
    let arr = Array.from(e);
    this.setState({
      files: update(this.state.files, {
        items: { $push: arr }
      })
    })
  }

  handleDeleteFile(el) {
    this.setState({
      files: update(this.state.files, {
        items: { $splice: [[parseInt(el.target.dataset.id), 1]] }
      })
    })
  }
  handleSendFrom() {
    let t = new FormData();

    let keys = Object.keys(this.state.item);

    keys.map( el => {
      if(el === 'fileinfo'){
        null
      } else if (el === 'apps') {
        t.append(el, JSON.stringify(this.state.item[el])) 
      } else { t.append(el, this.state.item[el]); }
    })

    if(Array.isArray(this.state.files.items) && this.state.files.items.length > 0){
      this.state.files.items.forEach((file, i)=>{
        t.append(`files`, file);
      })
    }
    
    api.post(`fls/instruction`, t, {
      s: ()=>{
        this.setState({
          redirect: true
        })
      }
    });
}
  render() {
    const { auth } = this.props;
    if (this.state.redirect) return <Redirect to={`/dashboard/settings/instr_list`}/>;
    return (
      <div className='ui form'>
        <div className='field'>
          <label>Заголовок</label>
          <input  type='text'
            value={ this.state.item ? this.state.item.title ? this.state.item.title : '' : '' }
            data-type='title'
            onChange={(e)=>this.handleChange(e)}
            placeholder='Заголовок' />
        </div>
        <div className='field'>
          <div className='five wide field'>
            <label>Приложение</label>
            <Select
              multy={true}
              label='Выберите приложение(ия)'
              source='api/apps'
              name='appname'
              id='id'
              value={ this.state.item.apps ? this.state.item.apps : [] }
              selected={ (result) => {
                // console.log(result);
                this.setState({
                  item: update(this.state.item, {
                    ['apps']: { $set: result }
                  })
                });
              }}
            />
          </div>
        </div>
        <div className='field'>
          <label>Файлы</label>
          <FilePickerBlock
            buttonStyle='ui icon button grey basic'
            icon='paperclip'
            files={this.state.files.items ? this.state.files.items : []}
            showFiles={this.state.item.fileinfo ? this.state.item.fileinfo : []}
            handleDelete={ev=>this.handleDeleteFile(ev)}
            change={ev=>this.handleFiles(ev)} />
        </div>
        <div className='ui segment'>
          <button onClick={()=>this.handleSendFrom()} className='ui button green'>Сохранить</button>
        </div>
      </div>
    )
  }
}

Istr_one.propTypes = {
  auth: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Istr_one);