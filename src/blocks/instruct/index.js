import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import Pagination from 'rc-pagination';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import * as authAction from '../../redux/actions/auth';

const api = new Api();
const prefix = 'api';

class Instruct extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      pagination: {
        pagenum: 1,
        pagesize: 15,
        substr: ''
      },
      data: []
    });
    this.initItem(`instructions?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);
  }
  initItem(string) {
    const { dispatch } = this.props;
    let filterQuery='',
        self = this;

    dispatch(authAction.handlerChangePreloader(true).bind(this));

    if(!Utils.isEmptyObject(this.state.filters)){
        let keys = Object.keys(this.state.filters);
        keys.forEach((el,i)=>{
            if(Array.isArray(this.state.filters[el])) {
                if(this.state.filters[el].length>0) {
                    filterQuery+=`&${el}=[${this.state.filters[el]}]`
                } else {
                    filterQuery+=`&${el}=`
                }
            } else {
                filterQuery+=`&${el}=${this.state.filters[el]}`
            }
        })
    }
    api.get(`${prefix}/${string}${filterQuery}`, {}, {
        s: (res) => {
            self.setState({
                data: res.data.outjson,
                pagination: update(this.state.pagination, {
                    ['foundcount']: { $set: res.data.foundcount ? res.data.foundcount : 0 }
                })
            });
            dispatch(authAction.handlerChangePreloader(false).bind(this));
        }
    });
}
  componentDidMount() {
    $('.ui.accordion').accordion();
  }
  handlerChangePage(page){
    const { auth } = this.props;
    let servname = this.props.match.params.servicename;
    let compname = this.props.match.params.compname;
    let url = compname ? compname : servname
    let id = this.props.match.params.id;
    let method;
    auth.menu.map((el)=>{
        if (el.apiurl === url) {
            el.config ? el.config.func_table ? method = el.config.func_table :  method = null : method = null
        }
    });
    this.initItem(`${method}?pagenum=${page}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);

    this.setState({
        pagination: update(this.state.pagination,
            {
                ['pagenum']: { $set: page }
            }
        )
    })
}
  renderItems(){
    if(this.state.data.length > 0) {
      return this.state.data.map((val, i, arr)=>{
        return [
          <div className="title" key={ 'acc_tit' + i }>
            <i className="dropdown icon"></i>
            { val.title }
          </div>,
          <div className="content" key={ 'acc_con' + i }>
            <FilesRender item={{
              files: val.fileinfo
            }} />
          </div>
        ]
      })
    } else {
      return (
        <div>
          
        </div>
      )
    }
  }
  render() {
    const { auth } = this.props;
    return (
      <div>
        <div className='column'>
          <Pagination className='ant-pagination'
            style={{margin: '4px 0'}}
            current={ this.state.pagination.pagenum }
            defaultPageSize={ this.state.pagination.pagesize }
            total={ this.state.pagination.foundcount }
            onChange={ (page)=> this.handlerChangePage(page) }
            locale={{ items_per_page: '/странице',
                jump_to: 'Перейти', page: '',
                prev_page: 'Назад', next_page: 'Вперед',
                prev_5: 'Предыдущие 5', next_5: 'Следующие 5'}} />
        </div>
        <div className='ui styled fluid accordion'>
          {
            this.renderItems()
          }
        </div>
      </div>
    )
  }
}

const FilesRender = (props) => {
  if(props.item) {
      if(props.item.files) {
          if(Array.isArray(props.item.files) && props.item.files.length > 0) {
              return props.item.files.map((file,i)=> {
                  return (
                      <div className='item' key={i+'filess'}>
                          <div className='content'>
                              <a target='_blank' href={file.filepath}>
                                  {file.filename}
                              </a>
                          </div>
                      </div>
                  )
              })
          } else return <div>Файлов нет</div>
      } else return <div>Файлов нет</div>
  } else return null
}

Instruct.propTypes = {
  auth: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Instruct);