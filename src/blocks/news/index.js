import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import * as authAction from '../../redux/actions/auth';

const api = new Api();
const prefix = 'api';

class News extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      pagination: {
        pagenum: 1,
        pagesize: 15,
        substr: ''
      },
      data: []
    });
    this.initItem();
  }
  initItem(){
    let self = this;
    api.get(`${prefix}/news?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`, {}, {
      s: (res) => {
        self.setState({
          data: res.data.outjson
        });
      }
    });
  }
  renderItems(){
    if(this.state.data.length > 0) {
      return this.state.data.map((val, i)=>{
        console.log(val.created);
        return (
          <div className='newItem' key={i+i+'ss'}>
            <div className='fields'>
                <div className='twelve wide field'>
                  <h2>{ val.title }</h2>
                </div>
                <div className='four wide field'>
                  <span style={{ float: 'right', color: '#98999a' }}>
                      { dateFormater(val.created, true) }
                  </span>
                </div>
            </div>
            <div dangerouslySetInnerHTML={{__html: val.newstext}} />
          </div>
        )
      })
    } else {
      return (
        <div>
          Новостей нет
        </div>
      )
    }
  }
  render() {
    const { auth } = this.props;
    return (
      <div className='block_news'>
        {
          this.renderItems()
        }
      </div>
    )
  }
}

News.propTypes = {
  auth: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(News);

/**
 * Преобразование даты в вид 21 февраля 2018;
 * @param date дата в формате 2018-02-27T14:43:51.352953
 * @param watch (true, false) показать время или нет
 *
 * @returns дата в формате 27 февраля 2018 время (14:26)
 */
function dateFormater(date, watch) {
  let c = new Date(date);
  let d = [];
  var o = {};
  let dc = new Date();
  if(watch){
      o = {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          timezone: 'UTC',
          hour: 'numeric',
          minute: 'numeric'
      };
      d.y = c.getFullYear();
      d.m = c.getMonth();
      d.d = c.getDate();
      d.h = c.getHours();
      d.min = c.getMinutes();
      dc = new Date(d.y, d.m, d.d, d.h, d.min);
  } else {
      o = {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
      };
      d.y = c.getFullYear();
      d.m = c.getMonth();
      d.d = c.getDate();
      let dc = new Date(d.y, d.m, d.d);
  }

  return dc.toLocaleString('ru', o);
}