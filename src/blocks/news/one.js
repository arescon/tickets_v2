import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Editor } from '@tinymce/tinymce-react';

import Select from './../../box/select';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import * as authAction from '../../redux/actions/auth';

const api = new Api();
const prefix = 'api';

class NewsOne extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      item: {},
      redirect: false
    });
  }
  componentDidMount(){
    let self = this;
    api.get(`${prefix}/newsone?id=${this.props.id}`, {}, {
      s: (res) => {
        self.setState({
          item: res.data.outjson
        });
      }
    });
  }
  handleSend(e) {
    api.post(`${prefix}/news?id=${this.state.item.id ? this.state.item.id : null}`, {
      id: this.state.item.id ? this.state.item.id : null,
      title: this.state.item.title ? this.state.item.title : null,
      newstext: this.state.item.newstext ? this.state.item.newstext : null,
      apps: this.state.item.apps ? this.state.item.apps.length > 0 ? this.state.item.apps : null : null,
      entype: this.props.entype
    }, {
      s: ()=>{
        this.setState({
          redirect: true
        });
      }
    });
  }
  render() {
    const { auth } = this.props;
    if (this.state.redirect) return <Redirect to={`/dashboard/settings/${ this.props.entype === 1 ? 'info' : 'faqs_list' }`}/>;

    var value = this.state.item.newstext ? this.state.item.newstext : null
    return (
      <div className='block_news_s'>
        <form className="ui form">
          <h4 className="ui dividing header">Добавить новость</h4>
          <div className="field">
            <div className="two fields">
              <div className="field">
                <label>Заголовок новости</label>
                <input type="text" name="title" placeholder="Заголовок"
                  value={ this.state.item.title ? this.state.item.title : '' }
                  onChange={ (e)=>{
                    this.setState({
                      item: update(this.state.item, {
                        ['title']: { $set: e.target.value }
                      })
                    })
                  }} />
              </div>
              <div className="field">
                <label>Приложение</label>
                <Select
                  multy={true}
                  label='Выберите приложение(ия)'
                  source='api/apps'
                  name='appname'
                  id='id'
                  value={ this.state.item.apps ? this.state.item.apps : [] }
                  selected={ (result) => {
                    this.setState({
                      item: update(this.state.item, {
                        ['apps']: { $set: result }
                      })
                    });
                  }}
                />
              </div>
            </div>
          </div>
          <div className="field">
            <label>Содержимое</label>
            <Editor
              value={ value }
              init={{
                plugins: 'link image code',
                toolbar: 'undo redo | bold italic forecolor backcolor | alignleft aligncenter alignright | numlist bullist outdent indent | code'
              }}
              onChange={(e)=>{
                this.setState({
                  item: update(this.state.item, {
                    ['newstext']: { $set: e.target.getContent() }
                  })
                })
              }}
            />
          </div>
          <div className="ui submit button" onClick={e=>this.handleSend(e)}>Опубликовать</div>
        </form>
      </div>
    )
  }
}

NewsOne.propTypes = {
  auth: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(NewsOne);
