import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import * as authAction from '../../redux/actions/auth';
import Checkbox from './../globals/CheckForm';
import Select from './../globals/selectForm';
import ListForm from './../globals/ListForm';

import Api from '../../utils/';
const api = new Api();

/*
    &nbsp; - пробел
*/
class AddMenu extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            new_item: {
                id: null,
                ptitle: '',
                apiurl: '',
                roles: [1],
                parentid: null,
                icon: '',
                mtype: '',
                config: {}
            }
        })
        this.hundlerChangeNew = this.hundlerChangeNew.bind(this);
        this.hundlerSubmit = this.hundlerSubmit.bind(this);

    }
    hundlerChangeNew(el) {
        let key = el.currentTarget.dataset.key;
        let value = el.currentTarget.value;
        this.setState({
            new_item: update(this.state.new_item,
                {
                    [`${key}`]: { $set: value }
                }
            )
        });
    }
    hundlerSubmit(el) {
        const { dispatch } = this.props;
        let data = this.state.new_item;
        data.parentid = parseInt(this.props._id);

        api.post('api/menu', data, {
            s: (res) => {
                data.id = res.data._id;
                dispatch(authAction.handlerEditMenu(data).bind(this));
            }
        })
    }
    render() {
        return (
            <div className='ui segment'>
                <div className='ui form'>
                    <h4 className='ui dividing header'>Добавить пункт меню</h4>
                    <div className='field'>
                        <div className='fields'>
                            <div className='seven wide field'>
                                <label>Название</label>
                                <input type='text'
                                    value={this.state.new_item.ptitle ? this.state.new_item.ptitle : ''}
                                    data-key='ptitle'
                                    onChange={this.hundlerChangeNew}
                                    placeholder='Название' />
                            </div>
                            <div className='six wide field'>
                                <label>Путь</label>
                                <input type='text'
                                    value={this.state.new_item.apiurl ? this.state.new_item.apiurl : ''}
                                    data-key='apiurl'
                                    onChange={this.hundlerChangeNew}
                                    placeholder='путь' />
                            </div>
                            <div className='three wide field'>
                                <label>&nbsp;</label>
                                <button className='ui button green'
                                    data-new={1}
                                    onClick={this.hundlerSubmit}>Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class Menu extends Component {
    constructor(props) {
        super(props);
        const { id } = this.props;
        this.state = ({
            item: {
                id: 0,
                ptitle: '',
                apiurl: '',
                roles: [],
                parentid: null,
                icon: '',
                mtype: '',
                config: {}
            },
            id: null
        });
        this.hundlerChange = this.hundlerChange.bind(this);
        this.hundlerSubmit = this.hundlerSubmit.bind(this);
        this.hundlerToUrl = this.hundlerToUrl.bind(this);
        this.handlerDeleteMenu = this.handlerDeleteMenu.bind(this);
        this.hundlerChangeConfig = this.hundlerChangeConfig.bind(this);

        this.hundlerMethodsAdd = this.hundlerMethodsAdd.bind(this);
        this.hundlerMethodsRemove = this.hundlerMethodsRemove.bind(this);
        this.hundlerMethodsEdit = this.hundlerMethodsEdit.bind(this);
        this.hundlerMethodsClick = this.hundlerMethodsClick.bind(this);
    }
    componentWillUpdate() {
        const { auth, id } = this.props;
        if (id) {
            if (parseInt(id) !== this.state.id) {
                auth.menu.map((el) => {
                    if (el.id === parseInt(id)) {
                        this.setState({
                            item: el,
                            id: parseInt(id)
                        })
                    }
                })
            }
        }
    }
    hundlerChange(el) {
        let key = el.currentTarget.dataset.key;
        let value = el.currentTarget.value;
        this.setState({
            item: update(this.state.item,
                {
                    [`${key}`]: { $set: value }
                }
            )
        });
    }
    hundlerChangeConfig(el) {
        let key = el.currentTarget.dataset.key;
        let value = el.currentTarget.value;
        this.setState({
            item: update(this.state.item,
                {
                    config: {
                        [`${key}`]: { $set: value }
                    }
                }
            )
        });
    }
    hundlerSubmit(el) {
        const { dispatch } = this.props;
        let _new = el.currentTarget.dataset.new;
        let data;
        if (parseInt(_new) === 1) data = this.state.new_item; else data = this.state.item
        api.post('api/menu', data, {
            s: (res) => {
                dispatch(authAction.handlerEditMenu(data, res.data._id).bind(this));
            }
        })
    }
    handlerDeleteMenu(el) {
        const { dispatch } = this.props;
        let id = el.currentTarget.dataset._id;
        api.delete(`api/menu?id=${id}`, {
            s: (res) => {
                dispatch(authAction.handlerDeleteMenu(res.data._id).bind(this));
            }
        })
    }
    hundlerToUrl(el) {
        const { auth, id } = this.props;
        let _id = el.currentTarget.dataset._id;
        if (_id) {
            if (parseInt(_id) !== this.state.id) {
                auth.menu.map((el) => {
                    if (el.id === parseInt(_id)) {
                        this.setState({
                            item: el,
                            id: parseInt(_id)
                        })
                    }
                })
            }
        }
    }
    rootMenu(_id) {
        const { auth, dispatch, id, compname, servname } = this.props;

        return auth.menu.map((el) => {
            if (el.parentid === parseInt(_id))
                return (
                    <div key={`${el.id}rootMenuItem`} className='item'>
                        <div className='content'>
                            <button className='ui mini icon basic red button'
                                onClick={this.handlerDeleteMenu}
                                data-_id={el.id}
                                style={{ position: 'absolute', right: '3px' }}>
                                <i className='icon trash'></i>
                            </button>
                            <Link to={`/dashboard/${servname}/${compname}/${el.id}`}
                                data-_id={el.id}
                                onClick={this.hundlerToUrl}
                                className='header'>
                                &nbsp;{el.ptitle}
                            </Link>
                            <div className='description'>&nbsp;{el.apiurl}</div>
                        </div>
                    </div>
                )
        })
    }
    kroshki() {
        const { auth, dispatch, id, compname, servname } = this.props;
        let title;
        if (id) {
            auth.menu.map((el) => {
                if (el.id === parseInt(id)) title = el.ptitle;

            })
            return (
                <div className='sub header'>
                    /
                    <Link to={`/dashboard/${servname}/${compname}`}>Меню</Link>
                    /
                    {title}
                </div>
            )
        }
    }
    titleMenu() {
        const { auth, dispatch, id, compname, servname } = this.props;
        let title;
        if (id) {
            auth.menu.map((el) => {
                if (el.id === parseInt(id)) title = el.ptitle;
            })
            return (
                <h4 className='ui inverted header'>
                    {title}
                </h4>
            )
        }
    }
    formRender() {
        return (
            <div className='ui form'>
                <div className='ui grid'>
                    <div className='eight wide column'>
                        <div className='field'>
                            <label>Название</label>
                            <input type='text'
                                value={this.state.item.ptitle ? this.state.item.ptitle : ''}
                                data-key='ptitle'
                                onChange={this.hundlerChange}
                                placeholder='Название' />
                        </div>
                        <div className='field'>
                            <label>Путь</label>
                            <input type='text'
                                data-key='apiurl'
                                onChange={this.hundlerChange}
                                value={this.state.item.apiurl ? this.state.item.apiurl : ''}
                                placeholder='Ссылка' />
                        </div>
                        <div className='field'>
                            <label>Тип</label>
                            <input type='text'
                                data-key='mtype'
                                onChange={this.hundlerChange}
                                value={this.state.item.mtype ? this.state.item.mtype : ''}
                                placeholder='тип' />
                        </div>
                        <div className='field'>
                            <label>Роли</label>
                            <Checkbox label=''
                                source='api/roles'
                                name='rolename'
                                dicid='id'
                                value={this.state.item.roles ? this.state.item.roles : []}
                                checked={(result) => {
                                    this.setState({
                                        item: update(this.state.item,
                                            {
                                                ['roles']: { $set: result }
                                            }
                                        )
                                    })
                                }}
                            />
                        </div>
                        <div className='field'>
                            <div className='ui checkbox'>
                                <input  type='checkbox' 
                                        data-key='hidden'
                                        onChange={(e)=>{
                                            let key = e.currentTarget.dataset.key;
                                            let value = e.currentTarget.checked;
                                            this.setState({
                                                item: update(this.state.item,
                                                    {
                                                        config: {
                                                            [`${key}`]: { $set: value }
                                                        }
                                                    }
                                                )
                                            });
                                        }}
                                        value={
                                            this.state.item
                                            ? this.state.item.config
                                            ? this.state.item.config.hidden
                                            ? this.state.item.config.hidden
                                            : false
                                            : false
                                            : false
                                        } name='hidden' />
                                <label>Скрыть в основном меню</label>
                            </div>
                        </div>
                        <div className='field'>
                            <button className='ui button green'
                                data-new={0}
                                onClick={this.hundlerSubmit}>Сохранить</button>
                        </div>
                    </div>
                    <div className='eight wide column'>
                        <h4>Конфиг</h4>
                        {this.formConfig(this.state.item.mtype)}
                    </div>
                </div>
            </div>
        )
    }
    hundlerMethodsAdd(item) {
        if (this.state.item.config) {
            if (this.state.item.config.actions) {
                this.setState({
                    item: update(this.state.item,
                        {
                            config: {
                                actions: { $push: [item] }
                            }
                        }
                    )
                })
            } else {
                let arr = [];
                arr.push(item);
                this.setState({
                    item: update(this.state.item,
                        {
                            config: {
                                actions: { $set: arr }
                            }
                        }
                    )
                })
            }
        }
    }
    hundlerMethodsRemove(item, config) {
        let newState = this.state.item.config.actions.filter(i => {
            return +i[config.listFieldNames.id] != +item[config.listFieldNames.id];
        })

        this.setState({
            item: update(this.state.item,
                {
                    config: {
                        actions: { $set: newState }
                    }
                }
            )
        })
    }
    hundlerMethodsEdit(item, config) {
        var idx = this.state.item.config.actions.findIndex((element) => {
            return +element[config.listFieldNames.id] == +item[config.listFieldNames.id];
        })

        this.setState({
            item: update(this.state.item, {
                config: {
                    actions: {
                        [`${idx}`]: { $set: item }
                    }
                }
            })
        })
    }

    hundlerMethodsClick(item) {
        // console.log('onclick', item);
    }
    formConfig(mtype) {
        switch (mtype) {
            case 'list':
                return (
                    <div className='ui segment '>
                        <div className='ui segment'>
                            <label>Кнопка 'добавить'</label>
                            <div className='field'>
                                <label>Ссылка</label>
                                <input type='text'
                                    data-key='new_url'
                                    onChange={this.hundlerChangeConfig}
                                    value={this.state.item.config ? this.state.item.config.new_url ? this.state.item.config.new_url : '' : ''}
                                    placeholder='new_url' />
                            </div>
                        </div>
                        <div className='ui segment'>
                            <label>Таблица</label>
                            <div className='field'>
                                <label>Функция getAll</label>
                                <input type='text'
                                    data-key='func_table'
                                    onChange={this.hundlerChangeConfig}
                                    value={this.state.item.config ? this.state.item.config.func_table ? this.state.item.config.func_table : '' : ''}
                                    placeholder='func_table' />
                            </div>
                            <div className='field'>
                                <label>Дабл клик по строке (ссылка)</label>
                                <input type='text'
                                    data-key='double_url'
                                    onChange={this.hundlerChangeConfig}
                                    value={this.state.item.config ? this.state.item.config.double_url ? this.state.item.config.double_url : '' : ''}
                                    placeholder='double_url' />
                            </div>
                            <div className='ui segment'>
                                <ListForm id='listFormTest'
                                    title='Экшены строки'
                                    dataItems={this.state.item.config ? this.state.item.config.actions ? this.state.item.config.actions : [] : []}
                                    config={{
                                        allFields: {
                                            id: {
                                                langs: {
                                                    rus: 'ИД',
                                                    eng: 'ID'
                                                },
                                                orderby: 1,
                                                readonly: false,
                                                default: 0,
                                                type: 'int',
                                                editable: true
                                            },
                                            name: {
                                                langs: {
                                                    rus: 'Название',
                                                    eng: 'Title'
                                                },
                                                orderby: 2,
                                                readonly: false,
                                                default: '',
                                                required: true,
                                                type: 'string',
                                                editable: true
                                            },
                                            url: {
                                                langs: {
                                                    rus: 'Ссылка',
                                                    eng: 'url'
                                                },
                                                orderby: 3,
                                                readonly: false,
                                                required: false,
                                                default: '',
                                                type: 'string',
                                                editable: true,
                                                /*props: {
                                                    min:3,
                                                    max:20
                                                },*/
                                            },
                                            icon: {
                                                langs: {
                                                    rus: 'Иконка',
                                                    eng: 'Icon'
                                                },
                                                orderby: 4,
                                                readonly: false,
                                                default: '',
                                                type: 'string',
                                                // props: {
                                                //     step: 0.01
                                                // },
                                                editable: true
                                            },
                                            method: {
                                                langs: {
                                                    rus: 'Метод',
                                                    eng: 'Method'
                                                },
                                                orderby: 5,
                                                readonly: false,
                                                default: 'POST',
                                                type: 'string',
                                                // props: {
                                                //     min:100,
                                                //     max:200
                                                // },
                                                editable: true
                                            }
                                        },
                                        listFieldNames: {
                                            id: 'id',
                                            title: 'name',
                                            descr: 'url'
                                        }
                                    }}
                                    userLang='rus'
                                    listClasses='divided'
                                    editable={true}
                                    actions={{
                                        add: this.hundlerMethodsAdd,
                                        remove: this.hundlerMethodsRemove,
                                        edit: this.hundlerMethodsEdit,
                                        click: this.hundlerMethodsClick
                                        // edit: (item)=> this.edit(item),
                                    }} />
                            </div>
                        </div>
                    </div>
                );
            case 'getOne':
                return (
                    <div className='ui segment '>
                        <div className='ui segment'>
                            <label>Метод для сохранения</label>
                            <div className='field'>
                                <label>Ссылка</label>
                                <input type='text'
                                    data-key='method'
                                    onChange={this.hundlerChangeConfig}
                                    checked={this.state.item.config ? this.state.item.config.method ? this.state.item.config.method : '' : ''}
                                    placeholder='method' />
                            </div>
                        </div>
                    </div>
                );
            case 'ticket':
                return (
                    <div className='ui segment '>
                        <div className='ui segment'>
                            <label>get метод</label>
                            <div className='field'>
                                <label>Ссылка</label>
                                <input type='text'
                                    data-key='method'
                                    onChange={this.hundlerChangeConfig}
                                    checked={this.state.item.config ? this.state.item.config.method ? this.state.item.config.method : '' : ''}
                                    placeholder='method' />
                            </div>
                        </div>
                    </div>
                )
                break;
        }
    }
    render() {
        const { auth, dispatch, id, compname, servname } = this.props;
        if (!id) {
            return (
                <div>
                    <div className='ui segment'>
                        <h3 className='ui header'>
                            Настройка меню
                            <div className='sub header'>
                                /
                            </div>
                        </h3>
                    </div>
                    <div className='ui segment'>
                        <div className='ui relaxed divided list'>
                            {this.rootMenu(0)}
                            <AddMenu dispatch={dispatch} _id={id} />
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div>
                    <div className='ui segment'>
                        <h3 className='ui header'>
                            Настройка меню
                            {this.kroshki()}
                        </h3>
                    </div>
                    <div className='ui segment grey inverted'>
                        {this.titleMenu()}
                        {this.formRender()}
                    </div>
                    <div className='ui segment'>
                        <div className='ui relaxed divided list'>
                            {this.rootMenu(id)}
                            <AddMenu dispatch={dispatch} _id={id} />
                        </div>
                    </div>
                </div>
            );;
        }
    }
}

Menu.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Menu);