import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import * as authAction from '../../redux/actions/auth';

const api = new Api();
const prefix = 'api';

class Faqs extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      pagination: {
        pagenum: 1,
        pagesize: 15,
        substr: ''
      },
      data: []
    });
  }
  componentWillMount() {
    let self = this;
    api.get(`${prefix}/faqs?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`, {}, {
      s: (res) => {
        self.setState({
          data: res.data.outjson
        });
      }
    });
  }
  componentDidMount() {
    $('.ui.accordion').accordion();
  }
  renderItems(){
    if(this.state.data.length > 0) {
      return this.state.data.map((val, i, arr)=>{
        return [
          <div className="title" key={ 'acc_tit' + i }>
            <i className="dropdown icon"></i>
            { val.title }
          </div>,
          <div className="content" key={ 'acc_con' + i }>
            <p className="transition hidden" dangerouslySetInnerHTML={{__html: val.newstext}} />
          </div>
        ]
      })
    } else {
      return (
        <div>
          
        </div>
      )
    }
  }
  render() {
    const { auth } = this.props;
    return (
      <div className='ui styled fluid accordion'>
        {
          this.renderItems()
        }
      </div>
    )
  }
}

Faqs.propTypes = {
  auth: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Faqs);