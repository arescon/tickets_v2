import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import * as authAction from '../../redux/actions/auth';
import Checkbox from './../globals/CheckForm';
import Select from './../globals/selectForm';

import Api from '../../utils/';
const api = new Api();

/*
    &nbsp; - пробел
*/
class MethodEditor extends Component {
    constructor(props) {
        super(props);
        const { id } = this.props;
        this.state = ({
            item: {}
        });
    }
    componentWillUpdate() {
        const { auth, id } = this.props;
        
    }
    render() {
        const { auth, dispatch, id, compname, servname } = this.props;
        return (
            <div>
                
            </div>
        )
    }
}

MethodEditor.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(MethodEditor);