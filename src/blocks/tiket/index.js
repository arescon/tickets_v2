import React from 'react';
import update from 'immutability-helper';
import Popup from 'reactjs-popup';
import Modal from 'react-responsive-modal';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ApiFactory from './../../utils/index';
import Checkbox from './../globals/CheckForm';
import FilePickerBlock from './../globals/filepicker';
import TableWCP from './../../components/dash/TableWCP';
import Search from './../globals/Search';

import Select from './../../box/select';

import Api from '../../utils/';
const api = new Api();
const prefix = 'api';

/**
 * Преобразование даты в вид 21 февраля 2018;
 * @param date дата в формате 2018-02-27T14:43:51.352953
 * @param watch (true, false) показать время или нет
 *
 * @returns дата в формате 27 февраля 2018 время (14:26)
 */
function dateFormater(date, watch) {
    let c = new Date(date);
    let d = [];
    var o = {};
    let dc = new Date();
    if(watch){
        o = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            timezone: 'UTC',
            hour: 'numeric',
            minute: 'numeric'
        };
        d.y = c.getFullYear();
        d.m = c.getMonth();
        d.d = c.getDate();
        d.h = c.getHours();
        d.min = c.getMinutes();
        dc = new Date(d.y, d.m, d.d, d.h, d.min);
    } else {
        o = {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        d.y = c.getFullYear();
        d.m = c.getMonth();
        d.d = c.getDate();
        let dc = new Date(d.y, d.m, d.d);
    }

    return dc.toLocaleString('ru', o);
}

/* Весь блок комментариев */
class CommentsBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            notys: []
        };
    }
    initItems(s) {
        let self = this;
        api.get(`${prefix}/${s}`, {}, {
            s: (res) => {
                self.setState({
                    items: res.data.outjson
                });
            }
        });
    }
    componentDidUpdate(){
      if(this.props.auth) {
        if(Array.isArray(this.props.auth.notys)){
          if(this.props.auth.notys.length>0){
            if(this.props.auth !== this.state.notys){
              this.props.auth.notys.map((el)=>{
                if(el.tableid === this.props.id){
                  api.get(`api/setsended?id=${el.id}`, {}, {
                    s: ()=> {
                      this.setState({notys: this.props.auth});
                    }
                  });
                  this.initItems(`comments?id=${this.props.id}`);
                }
              })
            }
          }
        }
      }
    }
    render() {
        if(this.state.items) {
            return (
                <div key='dawer3' className='ui form'>
                    <NewCommentBlock
                      item={this.props.item}
                      items={this.state.items}
                      url='comments'
                      id={this.props.id}
                      initItemsComment={(string)=>this.initItems(string)}
                      exdate={ this.state.item ? this.state.item.exdate ? this.state.item.exdate : null : null }
                    />
                    <br/>
                    <div className='field'>
                        <RenderComments
                          {...this.props}
                          url='comments'
                          id={this.props.id}
                          initItemsComment={(string)=>this.initItems(string)}
                          items={ this.state.items ? this.state.items : [] }
                        />
                    </div>
                </div>
            )
        } else return null;
    }
}

/* блок нового комментария */
class NewCommentBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            new_comment: {
                files: [],
                ticketid: this.props.item.id ? this.props.item.id : null,
                parentid: this.props.parentTicketId ? this.props.parentTicketId : null
            },
            visible: this.props.visible ? this.props.visible : false
        })
        this.formRender = this.formRender.bind(this);
        if(this.props.items.length === 0 && this.props.id !==0 ) this.props.initItemsComment(this.props.url+'?id='+this.props.id);
    }
    componentDidUpdate(nextProps){
        if(nextProps.id !== this.props.id)
            this.props.initItemsComment(this.props.url+'?id='+this.props.id);
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            new_comment: update(this.state.new_comment, {
                parentid: { $set: nextProps.parentTicketId ? nextProps.parentTicketId : null }
            })
        });
    }
    handleFiles(e) {
        let arr = Array.from(e);
        this.setState({
            new_comment: update(this.state.new_comment, {
                files: { $push: arr }
            })
        })
    }
    handleDeleteFile(el) {
        this.setState({
            new_comment: update(this.state.new_comment, {
                files: { $splice: [[parseInt(el.target.dataset.id), 1]] }
            })
        })
    }
    handleSendComment() {
        let t = new FormData();
        let self = this;

        t.append(`ticketid`, this.props.item.id ? this.props.item.id : null);
        t.append(`parentid`, this.props.parentTicketId ? this.props.parentTicketId : null);
        t.append(`mestext`, this.state.new_comment.mestext ? this.state.new_comment.mestext : '');

        if(Array.isArray(this.state.new_comment.files) && this.state.new_comment.files.length > 0){
            this.state.new_comment.files.forEach((file, i)=>{
                t.append(`files`, file);
            })
        }
        api.post('fls/comment', t, {
            s: ()=>{
                this.setState({
                    new_comment: {
                        files: [],
                        ticketid: this.props.item.id ? this.props.item.id : null,
                        parentid: this.props.parentTicketId ? this.props.parentTicketId : null
                    },
                    visible: this.props.visible ? this.props.visible : false
                })
                this.props.initItemsComment(this.props.url+'?id='+this.props.id);
            }
        });
    }
    formRender(){
        if(this.state.visible){
            return (
                <div className='field padding10'>
                    <div className='field'>
                        <label>Добавление комментария</label>
                    </div>
                    <div className='field padding10'>
                        <textarea
                            rows='3'
                            onChange={(e)=>{
                                this.setState({
                                    new_comment: update(this.state.new_comment, {
                                        ['mestext']: { $set: e.target.value }
                                    })
                                })
                            }}
                            value={this.state.new_comment.mestext ? this.state.new_comment.mestext : ''}></textarea>
                    </div>
                    <div className='field'>
                        <FilePickerBlock
                            buttonStyle='ui icon button grey basic'
                            icon='paperclip'
                            files={this.state.new_comment.files ? this.state.new_comment.files : []}
                            handleDelete={ev=>this.handleDeleteFile(ev)}
                            change={ev=>this.handleFiles(ev)}/>
                    </div>
                    <button onClick={()=>this.handleSendComment()} className='ui green button tiny'>Отправить</button>
                    <button onClick={ev=>{this.setState({visible: false})}} className='ui grey button tiny'>Отмена</button>
                </div>
            )
        } else return null;
    }
    render() {
        if(this.props.exdate === null ){
            return (
                <div className='field'>
                    <div className='field'>
                        {(
                            !this.state.visible ? <button onClick={ev=>{this.setState({visible: true})}} className='ui green button tiny'>Комментировать</button> : null
                        )}
                    </div>
                    { this.formRender() }
                </div>
            )
        } else return null;
    }
}

/* рендер комментариев по тикет id */
class RenderComments extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            commentForComment: {},
            visible: 0,
            new_comment: {}
        })
    }
    componentDidMount(){
        $('.filespopup')
            .popup({
                inline     : true,
                hoverable  : true,
                position   : 'bottom left',
                delay: {
                show: 300,
                hide: 800
                }
            });
    }
    showModal(ev) {
        let id = ev.target.dataset.id
        this.setState({
            commentForComment: update(this.state.commentForComment, {
                parent: { $set: parseInt(id) }
            })
        },()=>{
            $('.ui.modal').modal('show');
        })
    }
    showFiles(comment, i_parent) {
      if(comment.files.length > 0)
        return (
          <span style={{
            margin: '0 15px'
          }}>
            <Popup trigger={<i className='paperclip icon filespopup'></i>}
              position='left center'
              closeOnDocumentClick>
                <div className='ui list'>
                  {
                    comment.files.map((file,i)=>{
                      return (
                        <div key={`file${i}comment${i_parent}`} className='item'>
                          <div className='content'>
                            <a target='_blank' href={file.filepath}>
                              {file.filename}
                            </a>
                          </div>
                        </div>
                      )
                    })
                  }
                </div>
            </Popup>
          </span>
        )
      else return <span style={{ padding: '0 25px' }}></span>;
    }

    handleFiles(e) {
      let arr = Array.from(e);
      this.setState({
          new_comment: update(this.state.new_comment, {
              files: { $push: arr }
          })
      })
    }
    handleDeleteFile(el) {
        this.setState({
            new_comment: update(this.state.new_comment, {
                files: { $splice: [[parseInt(el.target.dataset.id), 1]] }
            })
        })
    }
    
    handleSendComment(comment) {
      let t = new FormData();

      t.append(`ticketid`, this.props.id || null);
      t.append(`parentid`, comment.id || null);
      t.append(`mestext`, this.state.new_comment.mestext || '');

      if(Array.isArray(this.state.new_comment.files) && this.state.new_comment.files.length > 0){
          this.state.new_comment.files.forEach((file, i)=>{
              t.append(`files`, file);
          })
      }
      api.post('fls/comment', t, {
          s: ()=>{
              this.setState({
                  new_comment: {},
                  visible: 0
              })
              this.props.initItemsComment(this.props.url+'?id='+this.props.id);
          }
      });
    }

    renderNewCommend(comment) {
      return (
        <div className='field padding10'>
            <div className='field'>
                <label>Добавление комментария</label>
            </div>
            <div className='field padding10'>
                <textarea
                    rows='3'
                    onChange={(e)=>{
                        this.setState({
                            new_comment: update(this.state.new_comment, {
                                ['mestext']: { $set: e.target.value }
                            })
                        })
                    }}
                    value={this.state.new_comment.mestext ? this.state.new_comment.mestext : ''}></textarea>
            </div>
            <div className='field'>
                <FilePickerBlock
                    buttonStyle='ui icon button grey basic'
                    icon='paperclip'
                    files={this.state.new_comment.files ? this.state.new_comment.files : []}
                    handleDelete={ev=>this.handleDeleteFile(ev)}
                    change={ev=>this.handleFiles(ev)}/>
            </div>
            <button onClick={()=>this.handleSendComment(comment)} className='ui green button tiny'>Отправить</button>
            <button onClick={()=>{this.setState({visible: 0})}} className='ui grey button tiny'>Отмена</button>
        </div>
      )
    }
    renderChild(_comment) {
      let { items } = this.props;
      let arr = items.filter(comment => {
        if(_comment.id !== null) return parseInt(comment.parentid) == _comment.id; else return comment.parentid == _comment.id;
      });
      console.log(arr);
      if(arr.length > 0)
        return (
          <div className='field ui segment' style={{marginLeft: '20px', marginTop: '10px'}}>
            {
              this.renderForm(_comment.id)
            }
          </div>
        );
        else return null;
    }
    renderForm(parent) {
      let { items } = this.props;
      if(items) {
        if(Array.isArray(items) && items.length > 0) {
          let arr = items.filter(comment => {
            if(parent !== null) return parseInt(comment.parentid) == parent; else return comment.parentid == parent;
          });
          return arr.map((comment,i)=>{
            return (
              <div key={`${i}comment`} className='field commentOne' >
                <div className='fields'>
                  <div className='twelve wide field'>
                      <span className='who_comment'>{ comment.userfio ? comment.userfio : '' }</span>
                  </div>
                  <div className='four wide field'>

                    <span style={{ float: 'right', color: '#98999a' }}>
                      { dateFormater(comment.created, true) }
                      { this.showFiles(comment, i) }
                      <span
                        style={{
                          textDecoration: 'underline',
                          cursor: 'pointer'
                        }}
                        onClick={()=>this.setState({visible: comment.id})}
                      >Ответить</span>
                    </span>
                  </div>                 
                </div>
                <div className='field' style={{whiteSpace: 'pre-line' }} dangerouslySetInnerHTML={{ __html: comment.mestext ? comment.mestext : '' }}>
                </div>
                <div className='field'>
                  {
                    comment.id === this.state.visible ? this.renderNewCommend(comment) : null
                  }
                </div>
                {
                  this.renderChild(comment)
                }
              </div>
            )
          })
        } else return <p key='dawer6' key='mekdas'>Нет комментариев</p>;
      } else return <p key='mekdas7'>Нет комментариев</p>;
    }
    render() {
        return [
            this.renderForm(null)
        ]
    }
}

/* правый блок с административной информацией */
class TiketExec extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { item } = this.props;
        return (
            <div className='ui segment'>
                <ItemLabel
                    label='Дата создания:'
                    content={
                        item
                        ? item.created
                        ? dateFormater(item.created, true)
                        : 'Не определено'
                        : 'Не определено'
                    }
                />
                <br/>
                <ItemLabel
                    label='Дата принятия:'
                    content={
                        item
                        ? item.acceptdate
                        ? dateFormater(item.acceptdate, true)
                        : 'Не определено'
                        : 'Не определено'
                    }
                />
                <div className='ui inverted divider'></div>
                <ItemLabel
                    label='Ответственный:'
                    content={
                        item
                        ? item.acceptuserfio
                        ? item.acceptuserfio
                        : 'Не определено'
                        : 'Не определено'
                    }
                />
                <br/>
                <ItemLabel
                    label='Статус заявки:'
                    content={
                        item
                        ? item.stagename
                        ? item.stagename
                        : 'Не определено'
                        : 'Не определено'
                    }
                />
                <div className='ui inverted divider'></div>
                <ItemLabel
                    label='Категория заявки:'
                    content={
                        item
                        ? item.mname
                        ? item.mname
                        : 'Не определено'
                        : 'Не определено'
                    }
                />
            </div>
        )
    }
}
/* блок статуса цветная */
const StatusTicket = (props) => {
    let stage = [
      {
        id: 1,
        title: 'Открыт',
        color: 'green',
        date: 'created'
      },
      {
        id: 2,
        title: 'Принят ПЛП',
        color: 'pink',
        date: 'acceptdate'
      },
      {
        id: 3,
        title: 'Отправлен на ВЛП',
        color: 'yellow',
      },
      {
        id: 4,
        title: 'Принят ВЛП',
        color: 'blue',
        date: 'acceptdate'
      },
      {
        id: 5,
        title: 'Закрыт',
        color: 'grey',
        date: 'exdate'
      },
      {
        id: 6,
        title: 'Архив',
        color: 'black',
      }
    ]
    return stage.map((el)=>{
      if(el.id === props.item.stageid) {
        return (
          <div className={`ui ${el.color || ''} inverted segment`}>
            { el.title + ': ' }
            { el.date ? dateFormater(props.item[el.date], true) : null }
          </div>
        )
      } else return null;
    })
}

/*  описание:
    текст       */
const ItemLabel = (props) => {
    let { label, classN, content } = props;
    return (
        <div className='field'>
            <div className={`label_item_ticket ${classN ? classN : ''}`}>{ label ? label : '' }</div>
            <p>{content ? content : ''}</p>
        </div>
    )
}

const FilesRender = (props) => {
    if(props.item) {
        if(props.item.files) {
            if(Array.isArray(props.item.files) && props.item.files.length > 0) {
                return props.item.files.map((file,i)=> {
                    return (
                        <div className='item' key={i+'filess'}>
                            <div className='content'>
                                <a target='_blank' href={file.filepath}>
                                    {file.filename}
                                </a>
                            </div>
                        </div>
                    )
                })
            } else return <div>Файлов нет</div>
        } else return <div>Файлов нет</div>
    } else return null
}

class Tiket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item,
            type: 'comments'
        };
        this.props.initItems(this.props.method+'?id='+this.props.id);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({item: nextProps.item});
    }
    access(mestypeid) {
        let self = this;
        api.post(`${prefix}/ticketaccept`,{
            id: self.props.item.id,
            mestypeid: mestypeid
        }, {
            s: ()=>{
                self.props.initItems(self.props.method+'?id='+self.props.id);
            }
        });
    }
    handleVoVtoruyu(mestypeid) {
        let self = this;
        api.post(`${prefix}/ticketsend`,{
            id: self.props.item.id
        }, {
            s: ()=>{
                self.props.initItems(self.props.method+'?id='+self.props.id);
            }
        });
    }
    closeTicket(mestypeid) {
        let self = this;
        api.post(`${prefix}/ticketclose`,{
            id: self.props.item.id
        }, {
            s: ()=>{
                self.props.initItems(self.props.method+'?id='+self.props.id);
            }
        });
    }
    OpenTicket(mestypeid) {
      let self = this;
      api.post(`${prefix}/unclose`,{
          id: self.props.item.id
      }, {
          s: ()=>{
              self.props.initItems(self.props.method+'?id='+self.props.id);
          }
      });
    }
    handleVPervuyu(mestypeid) {
      let self = this;
      api.post(`${prefix}/backplp`,{
          id: self.props.item.id
      }, {
          s: ()=>{
              self.props.initItems(self.props.method+'?id='+self.props.id);
          }
      });
    }
    render() {
        if(this.state.item ) {
            return (
                <div className='field ui form'>
                    <div className='fields'>
                        <div className='ten wide field'>
                            <div className='field'>
                                <div className='tiket ui segment'>
                                    <div className='field'>
                                        <h2>{this.state.item ? this.state.item.title ? this.state.item.title : '' : ''}</h2>
                                    </div>
                                    <div className='inline three fields'>
                                        <ItemLabel
                                            label='Организация:'
                                            content={this.state.item ? this.state.item.orgname ? this.state.item.orgname : '' : ''}
                                        />
                                        <ItemLabel
                                            label='Клиент:'
                                            content={this.state.item ? this.state.item.userfio ? this.state.item.userfio : '' : ''}
                                        />
                                        <ItemLabel
                                            label='Приложение:'
                                            content={this.state.item ? this.state.item.appname ? this.state.item.appname : '' : ''}
                                        />
                                    </div>
                                    <ItemLabel
                                        label='Описание:'
                                        content={this.state.item ? this.state.item.mestext ? this.state.item.mestext : '' : ''}
                                    />
                                </div>
                            </div>
                            <div className='field' style={{padding: '10px 0px' }} >
                                <CommentsBlock
                                    auth={this.props.auth}
                                    item={ this.state.item ? this.state.item : {} }
                                    id={ this.state.item ? this.state.item.id ? this.state.item.id : 0 : 0 }
                                    url='comments'
                                />
                            </div>
                        </div>
                        <div className='six wide field'>
                            <div className='field'>
                                <Actions
                                    access={e=>this.access(e)}
                                    handleVoVtoruyu={e=>this.handleVoVtoruyu(e)}
                                    handleVPervuyu={e=>this.handleVPervuyu(e)}
                                    closeTicket={e=>this.closeTicket(e)}
                                    OpenTicket={e=>this.OpenTicket(e)}
                                    item={this.state.item}
                                    auth={this.props.auth}
                                    menuItem={this.props.menuItem} />
                                <div style={{marginRight: '60px'}}>
                                    <StatusTicket item={this.state.item} />
                                </div>
                            </div>
                            <div className='tiket ui segment'>
                                <h5 key='dawer2' className='ui header'>Номер заявки: <span className='ui header'>{this.state.item.id || NaN}</span></h5>
                                
                            </div>
                            <div className='tiket ui segment'>
                                <h5 key='dawer2' className='ui header'>Файлы:</h5>
                                <div className='ui relaxed divided list'>
                                    <FilesRender {...this.props}/>
                                </div>
                            </div>
                            <TiketExec item={this.state.item} />
                            <div className='tiket ui segment'>
                                <h5 key='dawer2' className='ui header'>Участники:</h5>
                                <div className='ui relaxed divided list'>
                                    <RenderMembers {...this.props}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else return null;
    }
}

class RenderMembers extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      items: [],
      member: 0,
      id: Math.floor(Math.random() * 100) + 1,
      modal: false
    })
  }
  componentDidMount() {
    this.getItem();
  }
  getItem() {
    api.get(`api/members`, {
      ticketid: this.props.id
    }, {
      s: (res) => {
        this.setState({
          items: res.data.outjson,
        });
      }
    });
  }
  renderItems() {
    if(this.state.items && Array.isArray(this.state.items)) {
      let rowLen = this.state.items.length;
      return this.state.items.map((item, i) => {
        if (rowLen === i + 1) {
          return [
            <p key={`it_${i}`}>{ item.userfio }</p>,
            <div key={`it2_${i}`} className='ui inverted divider'></div>
          ]
        } else {
          return [
            <p key={`it_${i}`}>{ item.userfio }</p>,
            <div key={`it2_${i}`} className='ui inverted divider'></div>
          ]
        }
      })
    }
  }
  sendPost() {
    api.post(`${prefix}/invitetoticket`,{
      user: this.state.member,
      ticketid: this.props.id
    }, {
      s: ()=>{
        this.getItem();
        this.setState({member: 0})
        this.toggleModal();
      }
    });
  }
  toggleModal() {
    this.setState({
      modal: !this.state.modal
    })
  }
  render() {
    return (
      <div>
        {
          this.renderItems()
        }
        <button
          className='circular ui icon button center'
            onClick={(ev)=>{
              ev.preventDefault();
              // $(`#${this.state.id}`).modal('show');
              this.toggleModal();
          }}
        >
          <i className='plus icon' />
          Добавить участника
        </button>

        <Modal
          center
          styles={{
            modal: {
              width: '100%',
              maxWidth: '800px'
            }
          }}
          open={this.state.modal}
          onClose={()=> this.toggleModal() }
        >
          <h2>Добавление участника</h2>
          <div className='content'>
            <div className='field'>
              <label>
                Пользователь
              </label>
            </div>
            <Search urlS='api/users?isactiv=true&substr='
              resultS='outjson'
              titleS='fam'
              descriptionS='im'
              styleS={{width: '100%'}}
              name='search_a21'
              defValue=''
              kostyl_value={this.state.member}
              placeHolder='Пользователь'
              selected={(result) => {
                this.setState({
                  member: result.id
                })
              }}
              cleared={() => {
                this.setState({
                  member: 0
                })
              }}
            />
          </div>
          <div style={{
            marginTop: '10px',
            float: 'right'
          }}>
            <div className='ui positive button' onClick={()=>this.sendPost()}>Добавить</div>
            <div className='ui button' onClick={()=>{
                this.setState({member: 0})
                this.toggleModal();
              }}
            >Отмена</div>
          </div>
        </Modal>
        <div className='ui modal' id={this.state.id}>
          <div className='header'>Добавление участника</div>
          <div className='content'>
            <div className='field'>
              <label>
                Пользователь
              </label>
            </div>
          </div>
          <div className='actions'>
            <div className='ui approve button' onClick={()=>this.sendPost()}>Добавить</div>
            <div className='ui cancel button' onClick={()=>{this.setState({member: 0})}}>Отмена</div>
          </div>
        </div>
      </div>
    )
  }
}

class Actions extends React.Component {
    constructor(props){
        super(props)
        this.state = ({
            mestypeid: ''
        })
    }
    componentDidMount() {
        /* скрыть блок если клик был не по нему */
       let _drop = document.getElementById('action_block_ticket');
       function removeTest(e) {
           if(!e.target.matches('#action_block_ticket, #action_block_ticket *'))
               if(_drop.classList.contains('visible')) {
                   _drop.classList.toggle('visible');
           }
       };
       window.addEventListener('click', removeTest);
    }
    handleShowBlock() {
        let _drop = document.getElementById('action_block_ticket');
        _drop.classList.toggle('visible');
    }
    actions() {
        if(Array.isArray(this.props.auth.roles)) {
            if(this.props.auth.roles.length > 0) {
                return this.props.auth.roles.map((el, _e)=>{
                    if(el === 2) { // admin 1th line
                        if(this.props.item)
                            if(this.props.item.stageid)
                                if(this.props.item.stageid === 1)
                                  return (
                                    <div className='field' key={`asd${_e}`}>
                                      <label>
                                        Категория заявки
                                      </label>
                                      <Select
                                        label='ss3'
                                        source='api/dicsel?tagname=mestypes'
                                        name='mname'
                                        id='id'
                                        value={ this.state.mestypeid ? this.state.mestypeid : '' }
                                        selected={ (result) => {
                                            this.setState({
                                                mestypeid: result
                                            })
                                        }}/>
                                      <button
                                        className='ui button green'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.access(this.state.mestypeid)}>
                                        Принять заявку
                                      </button>
                                    </div>
                                  )
                                if(this.props.item.stageid === 2)
                                  return [
                                    <div className='field' key={`asd${_e}`}>
                                      <button
                                        className='ui button blue'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.handleVoVtoruyu(this.state.mestypeid)}>
                                        Отправить в ВЛП
                                      </button>
                                    </div>,
                                    <div className='field'>
                                      <button
                                        className='ui button red'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.closeTicket(this.state.mestypeid)}>
                                        Закрыть заявку
                                      </button>
                                    </div>
                                  ]
                                if(this.props.item.stageid === 5)
                                  return [
                                    <div className='field'>
                                      <button
                                        className='ui button green'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.OpenTicket(this.state.mestypeid)}>
                                        Открыть заявку
                                      </button>
                                    </div>
                                  ]
                    } else if(el === 1) { // programmer 2th line
                        if(this.props.item)
                            if(this.props.item.stageid)
                                if(this.props.item.stageid === 1 || this.props.item.stageid === 3)
                                  return [
                                    <div className='field' key={`asd${_e}`}>

                                        <label>
                                            Категория заявки
                                        </label>
                                        <Select
                                            label='ss3'
                                            source='api/dicsel?tagname=mestypes'
                                            name='mname'
                                            id='id'
                                            value={ this.state.mestypeid ? this.state.mestypeid : '' }
                                            selected={ (result) => {
                                                this.setState({
                                                    mestypeid: result
                                                })
                                            }}/>
                                        <button
                                            className='ui button green'
                                            style={{marginTop: '10px', width: '100%'}}
                                            onClick={e=>this.props.access(this.state.mestypeid)}>
                                            Принять заявку
                                        </button>
                                    </div>
                                  ]
                                if(this.props.item.stageid === 4)
                                  return [
                                    <div className='field' key={`asd${_e}`}>
                                      <button
                                        className='ui button blue'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.handleVPervuyu(this.state.mestypeid)}>
                                        Отправить в ПЛП
                                      </button>
                                    </div>,
                                    <div className='field'>
                                      <button
                                        className='ui button red'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.closeTicket(this.state.mestypeid)}>
                                        Закрыть заявку
                                      </button>
                                    </div>
                                  ]
                                if(this.props.item.stageid === 5)
                                  return [
                                    <div className='field' key={`asd${_e}`}>
                                      <button
                                        className='ui button green'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.OpenTicket(this.state.mestypeid)}>
                                        Открыть заявку
                                      </button>
                                    </div>
                                  ]
                    } else {
                      if(this.props.item)
                            if(this.props.item.stageid)
                                if(this.props.item.stageid === 5)
                                  return [
                                    <div className='field' key={`asd${_e}`}>
                                      <button
                                        className='ui button green'
                                        style={{marginTop: '10px', width: '100%'}}
                                        onClick={e=>this.props.OpenTicket(this.state.mestypeid)}>
                                        Открыть заявку
                                      </button>
                                    </div>
                                  ]
                    }
                })
            }
        }
    }
    render() {
        return (
            <div id='action_block_ticket' className=''>
                <div className='field'>
                    <div className='ui segment inverted grey visible_button' onClick={e=>this.handleShowBlock(e)} >
                        <i className='ui icon cog large' />
                    </div>
                </div>
                <div className='field options hidden'>
                    <div className='ui segment inverted grey'>
                        { this.actions() }
                    </div>
                </div>
            </div>
        )
    }
}


Tiket.propTypes = {
  auth: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
      auth: state.auth
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Tiket);
