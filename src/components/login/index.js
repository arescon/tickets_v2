import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Api from '../../utils/';
import { Redirect } from "react-router-dom";
const api = new Api();

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            injson: {
                login: '',
                password: ''
            },
            error: false,
            err_mess: '',
            redirect: false
        })
    }

    login() {
        api.post('auth/login', this.state.injson, {
            s: (res) => {
                // this.props.dispatch(go_auth(res.data ? res.data : res));
                this.setState({
                    injson: {
                        login: '',
                        password: ''
                    },
                    redirect: true
                });
            },
            e: (err) => {
                // console.log(err);
                this.setState({
                    error: true,
                    err_mess: err.response ? err.response.data ? err.response.data.message : 'Ошибка не определена, обратитесь в службу поддержки' : 'Ошибка не определена, обратитесь в службу поддержки'
                })
            }
        })
    }

    handleValue(e){
        this.setState({
            injson: update(this.state.injson,
                {
                    [`${e.currentTarget.dataset.name}`]: {$set: e.currentTarget.value}
                }
            )
        })
    }

    _handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.login();
        }
    }

    render() {
        const { auth } = this.props;

        if (this.state.redirect) return <Redirect to='/dashboard'/>;

        return (
            <div className='wrapper back_home'>
                <div className='page-login'>
                    <div className='ui centered grid container'>
                        <div className='nine wide column'>
                            <div className='ui fluid card'>
                                <div className='content'>
                                    <div className='ui form'>
                                        <div className='field'>
                                            <label>Логин</label>
                                            <input type='text'
                                                   name='user'
                                                   data-name='login'
                                                   placeholder='Логин'
                                                   value={this.state.injson.login}
                                                   onKeyPress={ ::this._handleKeyPress }
                                                   onChange={ ::this.handleValue }/>
                                        </div>
                                        <div className='field'>
                                            <label>Пароль</label>
                                            <input type='password'
                                                   name='pass'
                                                   data-name='password'
                                                   placeholder='Пароль'
                                                   onKeyPress={ ::this._handleKeyPress }
                                                   value={this.state.injson.password}
                                                   onChange={ ::this.handleValue }/>
                                        </div>
                                        <button className='ui primary labeled icon button' onClick={ ::this.login }>
                                            <i className='sign in alternate icon'/>
                                            Войти
                                        </button>
                                        <Link to='/' className='ui basic button'>
                                            Главная
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            {
                                this.state.error === true ? <div className='ui icon warning message'>
                                    <i className='lock icon'/>
                                    <div className='content'>
                                        <div className='header'>
                                            Ошибка авторизации
                                        </div>
                                        <p>{this.state.err_mess}</p>
                                    </div>
                                </div> : null
                            }
                        </div>
                    </div>
                </div>
                <style>{'\
                    body {\
                        background-color: #868686;\
                    }\
                    .page-login {\
                        margin-top: 25px;\
                    }\
                '}</style>
            </div>
        )
    }
}

Login.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Login);