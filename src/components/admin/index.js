import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'; 

class Admin extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { list } = this.props;

        return (
            <div className='wrapper'>
                <div className='ui segment'>
                    Админка   
                </div>
            </div>
        )
    }
}

Admin.propTypes = {
    list: PropTypes.any,
    type: PropTypes.number,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        list: state.list
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Admin);