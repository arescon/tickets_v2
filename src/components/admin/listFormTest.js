import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import ListForm from '../../blocks/globals/ListForm';

class ListFormTest extends Component {
    constructor(props) {
        super(props);

        this.edit = this.edit.bind(this);
        this.remove = this.remove.bind(this);
        this.add = this.add.bind(this);

        this.state = {
            dataItems: [
                {
                    id: 1,
                    ptitle: 'one',
                    url: 'http:\\one',
                    sum: 11
                },
                {
                    id: 2,
                    ptitle: 'one 2',
                    url: 'http:\\one2',
                    sum: 22
                },
                {
                    id: 3,
                    ptitle: 'one3',
                    url: 'http:\\one3',
                    sum: 33
                },
                {
                    id: 4,
                    ptitle: 'one 4',
                    url: 'http:\\one4',
                    sum: 44
                },
            ],
            config: {
                allFields: {
                    id: {
                        langs: {
                            rus: 'ИД',
                            eng: 'ID'
                        },
                        orderby: 1,
                        readonly: true,
                        default: 0,
                        type: 'int',
                        editable:true
                    },
                    ptitle: {
                        langs: {
                            rus: 'Наименование',
                            eng: 'Title'
                        },
                        orderby: 2,
                        readonly: false,
                        default: '',
                        required:true,
                        type: 'string',
                        editable:true
                    },
                    url: {
                        langs: {
                            rus: 'Описание',
                            eng: 'Description'
                        },
                        orderby: 3,
                        readonly: false,
                        required:false,
                        default: 'http://',
                        type: 'string',
                        editable:true,
                        props: {
                            min:3,
                            max:20
                        },
                    },
                    sum: {
                        langs: {
                            rus: 'Сумма',
                            eng: 'Sum'
                        },
                        orderby: 4,
                        readonly: false,
                        default: 0,
                        type: 'numeric',
                        props: {
                            step: 0.01
                        },
                        editable:true
                    },
                    num: {
                        langs: {
                            rus: 'Число',
                            eng: 'Num'
                        },
                        orderby: 5,
                        readonly: false,
                        default: 100,
                        type: 'int',
                        props: {
                            min:100,
                            max:200
                        },
                        editable:true
                    }
                },
                listFieldNames: {
                    id: 'id',
                    title: 'ptitle',
                    descr: 'url'
                }
            },
            userLang: 'rus'
        }
    }

    add(item) {
        // console.log('add', item);

        item[this.state.config.listFieldNames.id] = Math.round(Math.random() * 100);
        this.setState({
            dataItems: update(this.state.dataItems,
                {
                    $push: [item]
                }
            )
        })

    }
    remove(item) {
        let newState = this.state.dataItems.filter(i => {
            return +i[this.state.config.listFieldNames.id] != +item[this.state.config.listFieldNames.id];
        })

        this.setState({
            dataItems: newState
        })
    }
    edit(item) {
        //// console.log(item);

        // in api
        // s: >>

        var idx = this.state.dataItems.findIndex((element) => {
            return +element[this.state.config.listFieldNames.id] == +item[this.state.config.listFieldNames.id];
        })
        // index по id

        this.setState({
            dataItems: update(this.state.dataItems,
                {
                    [`${idx}`]: { $set: item }
                }
            )
        })
    }
    
    click(item) {
        // console.log('onclick', item);
    }

    render() {
        return (
            <div>
                <h1>listformTest</h1>
                <div style={{ width: '500px' }}>
                    <div className="ui grid">
                        <div className="sixteen wide column">
                            <ListForm
                                id="listFormTest"
                                dataItems={this.state.dataItems}
                                config={this.state.config}
                                userLang={this.state.userLang}
                                listClasses='divided'
                                editable={true}
                                actions={{
                                    add: this.add,
                                    remove: this.remove,
                                    edit: this.edit,
                                    click: this.click
                                    // edit: (item)=> this.edit(item),
                                }} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// Header.propTypes = {
//     list: PropTypes.any,
//     dispatch: PropTypes.func.isRequired
// };

// const mapStateToProps = state => {
//     return {
//         list: state.list
//     };
// };

export default ListFormTest; //connect(mapStateToProps, dispatch => ({ dispatch }))(ListFormTest);