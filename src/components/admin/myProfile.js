import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import * as authAction from '../../redux/actions/auth';

const api = new Api();

class MyProfile extends Component {
    constructor(props) {
      super(props);
      this.state = ({
        redirect: false,
        telegram: '',
        notys: {}
      })
      let self = this;
      api.get(`api/gentelegramid`, {}, {
        s: (res) => {
          self.setState({
            telegram: res.data.outjson
          });
        }
      });
      api.get(`api/usersettings`, {}, {
        s: (res) => {
          self.setState({
            notys: res.data.outjson
          });
        }
      });
    }
    handlerNotisy(mail, telega) {
      api.post(`api/usersettings`,{
        getmailnotif: mail,
        gettelegramnotif: telega
      }, null, 'Сохранено');
    }
    render() {
      const { auth } = this.props;
      if (this.state.redirect) return <Redirect to='/' />;
      return (
        <div className='ui container' >
          <p></p>
          <h2 className="ui header">Личный кабинет</h2>
          <div className="ui vertically divided grid segment">
            <div className="two column row">
              <div className="column">
                <img className='telegram_icon' src='https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/2000px-Telegram_logo.svg.png'></img>
                <h3 style={{marginTop: '0'}}>Привязка Telegram</h3>
                {
                  this.state.telegram ? this.state.telegram.isrelate ? 
                    <div>
                      <p>Привязан аккаунт:</p>
                      <span>
                        {
                          this.state.telegram.telusername ?
                          <span style={{ borderBottom: '1px solid black' }}>{ this.state.telegram.telusername }</span>
                          : null
                        }
                      </span>
                    </div>
                  : 
                  <div>
                    <p>Для получения уведомлений из системы в Телеграм:<br/>
                    1. Найдите нашего бота по ссылке <a target='_blank' href='http://t.me/jelatabot'>@JelataBot</a><br/>
                    2. Введите полученный идентификатор в телеграме.</p><br/>
                    <div className="ui form">
                      <div className="inline fields">
                        <div className="sixteen wide field">
                          <label>Код подтверждения</label>
                          <input type="text" placeholder="Код подтверждения Telegram" defaultValue={ this.state.telegram.num ? this.state.telegram.num : '' } />
                        </div>
                      </div>
                    </div>
                  </div>
                  : null
                }
              </div>
              <div className="column">
                <h4>Настройка уведомлений</h4>
                <div className="ui form">
                  <div className="fields">
                    <div className="sixteen wide field">
                      <div className="row">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={ this.state.notys.getmailnotif || false }
                              onChange={(ev)=>{
                                this.setState({
                                  notys: update(this.state.notys, {
                                    getmailnotif: { $set: !this.state.notys.getmailnotif }
                                  })
                                })
                                this.handlerNotisy(!this.state.notys.getmailnotif, this.state.notys.gettelegramnotif)
                              }}
                            />
                            Уведомления на электронную почту
                          </label>
                        </div>
                      </div>
                      <div className="row">
                        <div className="checkbox">
                          <label>
                            <input
                              type="checkbox"
                              checked={ this.state.notys.gettelegramnotif || false }
                              onChange={(ev)=>{
                                this.setState({
                                  notys: update(this.state.notys, {
                                    gettelegramnotif: { $set: !this.state.notys.gettelegramnotif }
                                  })
                                })
                                this.handlerNotisy(this.state.notys.getmailnotif, !this.state.notys.gettelegramnotif)
                              }}
                            />
                            Уведомления на Telegram
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="three column row">
              <div className="column">
                <p></p>
              </div>
              <div className="column">
                <p></p>
              </div>
              <div className="column">
                <p></p>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

MyProfile.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(MyProfile);