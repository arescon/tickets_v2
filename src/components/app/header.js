import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'; 

class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className='header'>
                header
            </div>
        );
    }
}

Header.propTypes = {
    list: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        list: state.list
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Header);