import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class App extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { list } = this.props;

        return (
            <div className='wrapper'>
                <div className='ui inverted vertical masthead center aligned segment back_home'>
                    <div className='ui container'>
                        <div className='ui large secondary inverted pointing menu'>
                            <h2>Система поддержки пользователей</h2>
                        </div>
                    </div>
                    <div className='row text-center center_home'>
                        <Link to='/dashboard'><h1 className='fh5co-text'>Войти</h1></Link>
                    </div>
                </div>
            </div>
        )
    }
}

App.propTypes = {
    list: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        list: state.list
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(App);