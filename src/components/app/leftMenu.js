import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import MyMenu from '../../blocks/MyMenu';

class LeftMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='left_Menu'>
                <MyMenu />
            </div>
        );
    }
}

LeftMenu.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(LeftMenu);
