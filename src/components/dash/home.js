import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import Api from '../../utils/';
import Utils from '../../utils/utils';
const api = new Api();

const HomeDash = (props) => {
  let { auth } = props;
  // console.log(props)
  return (
    <div className='ui container'>
      <br/><br/>
      <div className="ui three stackable cards">
          <div className="card inverted green">
              <div className="content">
                  <div className="header">Всего</div>
                  <div className="meta">Общее число заявок</div>
                  <div className="description">
                      { auth.dashboard ? auth.dashboard.alltickets ? auth.dashboard.alltickets : null : null }
                  </div>
              </div>
          </div>
          <div className="card inverted yellow">
              <div className="content">
                  <div className="header">В обработке</div>
                  <div className="meta">Заявки находящиеся в обработке</div>
                  <div className="description">
                    { auth.dashboard ? auth.dashboard.inwork ? auth.dashboard.inwork : null : null }
                  </div>
              </div>
          </div>
          <div className="card inverted red">
              <div className="content">
                  <div className="header">Закрытые</div>
                  <div className="meta">Закрытые заявки</div>
                  <div className="description">
                    { auth.dashboard ? auth.dashboard.closed ? auth.dashboard.closed : null : null }
                  </div>
              </div>
          </div>
      </div>
    </div>
  )
}

HomeDash.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(HomeDash);