import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import * as authAction from '../../redux/actions/auth';
import LeftMenu from './leftMenu';

import Header from './header';

class DashLayout extends Component {
    constructor(props) {
      super(props);
      const { dispatch } = this.props;
      dispatch(authAction.getMenuUser().bind(this));
      this.state = ({
          redirect: false
      });
    }
    render() {
        const { auth } = this.props;
        const servname = this.props.children.props.match.params.servicename;
        if (servname){
            return [
                <div key='ldr1' className='ui loading segment' style={{
                  bottom: '0px',
                  height: '100%',
                  width: '100%',
                  position: 'fixed',
                  background: '#000',
                  zIndex: '999',
                  display: auth.loading ? 'block' : 'none'
                }}>
                    <p/><p/>
                </div>,
                <div key='dash_lyt' className='ui dash'>
                    <Header thisLocation={servname}/>
                    <div className='ui'>
                        { servname !== 'tikets' ?
                            <LeftMenu children={this.props.children}
                                        servicename={servname}
                                        compname={servname}/>
                            : null }
                        <div className={
                            servname === 'tikets' ? 'main-content-full' : 'main-content'
                        }>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            ]
        } else {
            return [
                <div key='ldr1' className='ui loading segment' style={{
                    bottom: '0px',
                    height: '100%',
                    width: '100%',
                    position: 'fixed',
                    background: '#000',
                    zIndex: '999',
                    display: auth.loading ? 'block' : 'none'
                }}>
                    <p/>
                    <p/>
                </div>,
                <div key='dash_lyt2' className='ui dash'>
                    <Header thisLocation={servname}/>
                    {this.props.children}
                </div>
            ]
        }
    }
}

DashLayout.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(DashLayout);