import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import Sticky from 'react-sticky-el';
import Select from './../../box/select';

import tableStyles from './table.scss';

class Filters extends React.Component {
    constructor(props) {
        super(props)
        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1;
        var curr_year = d.getFullYear();

        if(curr_month <10) curr_month = `0${curr_month}`
        if(curr_date <10) curr_date = `0${curr_date}`
        this.state = ({
            users: {
                isactiv: {
                    type: 'checkbox',
                    name: 'Активен',
                    default: false
                }
            },
            tikets: {
                begdate: {
                    type: 'date',
                    name: 'Дата создания с:',
                    default: '2018-01-01'
                },
                enddate: {
                    type: 'date',
                    name: 'по',
                    default: `${curr_year}-${curr_month}-${curr_date}`
                },
                statuses: {
                    type: 'select',
                    name: 'Статус',
                    dic: 'api/dicsel?tagname=stage',
                    dicname: 'sname',
                    dicid: 'id',
                    default: []
                },
                apps: {
                    type: 'select',
                    name: 'Приложения',
                    dic: 'api/apps',
                    dicname: 'appname',
                    dicid: 'id',
                    default: []
                },
                orgs: {
                    type: 'select',
                    name: 'Организации',
                    dic: 'api/userorgs',
                    dicname: 'orgname',
                    dicid: 'id',
                    default: []
                }
            }
        })
    }
    componentWillMount() {
        let { menuItem, cur_servname, before_servname } = this.props;
        if(cur_servname || before_servname) {
          if(cur_servname !== before_servname) {
            if(menuItem) this.props.handlerFiltersDef(this.state[menuItem.apiurl] ? this.state[menuItem.apiurl] : {})
          }
        };
    }
    handleChangeFilterVal(filter, val) {
        let obj = new Object();
        obj.filter = filter ? filter : '';
        obj.val = val ? val : '';
        this.props.handleChangeFilterVal(obj);
    }
    renderFrom(apiurl) {
        if(this.state[apiurl]) {
            let keys = Object.keys(this.state[apiurl]);
            if(keys.length > 0 )
                return keys.map((el,i)=>{
                    if(this.state[apiurl][el].type)
                        if(this.state[apiurl][el].type === 'checkbox') {
                            return (
                                <div className='field' key={i+'asdsad'}>
                                    <div className='ui checkbox'>
                                        <input
                                            type='checkbox'
                                            data-filter={el}
                                            checked={this.props.filters[el] ? this.props.filters[el] : false}
                                            onChange={ev=>this.props.handleChangeFilter(ev)}
                                        />
                                        <label>{this.state[apiurl][el].name}</label>
                                    </div>
                                </div>
                            )
                        } else if(this.state[apiurl][el].type === 'date') {
                            return (
                                <div className='field' key={i+'date'}>
                                        <label>{this.state[apiurl][el].name}</label>
                                        <input
                                            type='date'
                                            data-filter={el}
                                            value={this.props.filters[el] ? this.props.filters[el] : ''}
                                            onChange={ev=>{
                                                this.handleChangeFilterVal(ev.target.dataset.filter, ev.target.value)
                                            }}
                                        />
                                </div>
                            )
                        } else if(this.state[apiurl][el].type === 'select') {
                            return (
                                <div className='field' key={i+'asdsad'}>
                                    <label>{ this.state[apiurl][el].name ? this.state[apiurl][el].name : '' }</label>
                                    <Select
                                        multy={true}
                                        label={this.state[apiurl][el].name ? this.state[apiurl][el].name : ''}
                                        source={ this.state[apiurl][el].dic ? this.state[apiurl][el].dic : '' }
                                        name={ this.state[apiurl][el].dicname ? this.state[apiurl][el].dicname : '' }
                                        id={ this.state[apiurl][el].dicid ? this.state[apiurl][el].dicid : '' }
                                        value={ this.props.filters[el] ? this.props.filters[el] : [] }
                                        selected={ (result) => {
                                            this.handleChangeFilterVal(el, result)
                                        }}/>
                                </div>
                            )
                        }
                })
        }
    }
    render() {
        let { menuItem } = this.props;
        if(menuItem) {
            if(menuItem.apiurl==='users' || menuItem.apiurl==='tikets') {
                return (
                    <div className='ui form'>
                        <div className='fields'>
                            { this.renderFrom(menuItem.apiurl) }
                        </div>
                        <button onClick={()=>this.props.initItemFilter()} className='ui button blue basic'>Применить фильтры</button>
                        <button onClick={()=>this.props.handlerFiltersDef(this.state[menuItem.apiurl] ? this.state[menuItem.apiurl] : {})} className='ui button red basic'>Очистить фильтры</button>
                        {
                            this.props.url === 'tikets'
                            ? <button onClick={()=>this.props.handlerPrintReport()} className='ui button blue basic'>Печать отчета</button>
                            : null
                        }
                    </div>
                )
            } else return null;
        } else return null;
    }
}

export default class TableWCP extends React.Component {
    constructor(props) {
        super(props);
        this.initItem();
    }
    initItem() {
        if(this.props.initItems){
            if(this.props.pagination){
                this.props.initItems(`${this.props.method}?pagenum=${this.props.pagination ? this.props.pagination.pagenum : 1}&pagesize=${this.props.pagination ? this.props.pagination.pagesize : ''}&substr=${this.props.pagination ? this.props.pagination.substr : ''}`);
            } else {
                this.props.initItems(`${this.props.method}`);
            }
        }
    }
    initItemFilter() {
        this.props.handlerChangePageDef();
        if(this.props.initItems){
            if(this.props.pagination){
                this.props.initItems(`${this.props.method}?pagenum=1&pagesize=${this.props.pagination ? this.props.pagination.pagesize : ''}&substr=${this.props.pagination ? this.props.pagination.substr : ''}`);
            } else {
                this.props.initItems(`${this.props.method}`);
            }
        }
    }
    componentDidMount(){
        $('.ui.dropdown').dropdown();
    }
    componentDidUpdate(){
        $('.ui.dropdown').dropdown();
    }
    componentWillReceiveProps() {
        $('.ui.dropdown').dropdown();
    }
    rowClick(e) {
        this.props.rowClick ? this.props.rowClick(e.currentTarget.dataset.id) : null;
    }
    rowDoubleClick(e) {
      if(this.props.rowDoubleClick){
          this.props.menuConfig
              ? this.props.menuConfig.double_url
              ? this.props.rowDoubleClick(e.currentTarget.dataset.id, this.props.menuConfig.double_url)
              : null
              : null
      }
    }
    render() {
        const {
            config,
            rowClick,
            rowDoubleClick,
            actions,
            actionFunc,
            currentLocation
        } = this.props;

        let t = Object.keys(config);

        let items = Array.isArray( this.props.items ) ? this.props.items : []; // костыль

        return [
            <Filters
                menuItem={this.props.menuItem}
                initItem={()=>this.initItem()}
                initItemFilter={()=>this.initItemFilter()}
                cur_servname={this.props.cur_servname}
                before_servname={this.props.before_servname}
                filters={ this.props.filters }
                url={this.props.url}
                handlerPrintReport={ev=>this.props.handlerPrintReport(ev)}
                handlerFiltersDef={ev=>this.props.handlerFiltersDef(ev)}
                handleChangeFilter={ev=>this.props.handleChangeFilter(ev)}
                handleChangeFilterVal={obj=>this.props.handleChangeFilterVal(obj)}/>,
            <table className='ui selectable celled striped very small table tableS'>
                <thead>
                    <tr>
                        {
                            t.map(i => {
                                if (config[i].type !== 'hidden') {
                                    return <th className='single line' key={i}>{config[i].title ? config[i].title : ''}</th>
                                }
                            })
                        }
                        {
                            (actions && actions.length > 0) ?
                                <th className='single line'>Действия</th>
                                : null
                        }
                    </tr>
                </thead>
                <tbody>
                {
                    items.map(i => {
                        return (
                            <tr key={ i.id + 'ss' }
                                data-id={ i.id }
                                onDoubleClick={ (rowDoubleClick !== undefined) ? ::this.rowDoubleClick : null  }
                                onClick={(rowClick !== undefined) ? ::this.rowClick : null }>
                                {
                                    t.map(k => {
                                        if (config[k].type === 'id') {
                                            return (
                                                <td key={i + k}>
                                                    { i[k] }
                                                </td>
                                            )
                                        } else if (config[k].type === 'text') {
                                            return (
                                                <td key={i + k}>
                                                    { i[k] }
                                                </td>
                                            )
                                        } else if (config[k].type === 'status') {
                                            return (
                                                <td key={i + k} className={`status s${i[config[k].istitle]}`} >
                                                    { i[k] }
                                                </td>
                                            )
                                        } else if (config[k].type === 'array') {
                                            return (
                                                <td key={i + k}>
                                                    <ol>
                                                    {
                                                        i[k].map((el,index1)=>{
                                                            return <li key={index1}>{el[config[k].dicid]}</li>
                                                        })
                                                    }
                                                    </ol>
                                                </td>
                                            )
                                        }
                                    })
                                }
                                {
                                    (actions && actions.length > 0) ?
                                        (
                                            <td>
                                                <div className='ui right pointing dropdown icon button'>
                                                    <i className='wrench icon'/>
                                                    <div className='menu'>
                                                        <div className='header'>Действия</div>
                                                        {
                                                            actions.map((act, k) => {
                                                                if (act.method === 'GET') {
                                                                    return (
                                                                        <Link   key={i.id + '_' + k}
                                                                                to={`${currentLocation}/${act.url}/${i.id}`}
                                                                                title={act.name}
                                                                                className='circular ui link icon item'>
                                                                            <i className={'icon ' + act.icon}/>
                                                                            {act.name}
                                                                        </Link>
                                                                    )
                                                                } else {
                                                                    return (
                                                                        <div
                                                                            key={i.id + '_' + k}
                                                                            className='circular ui link icon item'
                                                                            data-id={i.id}

                                                                            title={act.name}
                                                                            data-el={JSON.stringify(act)}
                                                                            data-func_name={this.props.method ? this.props.method : ''}
                                                                            onClick={actionFunc}>
                                                                            <i className={'icon ' + act.icon}/>
                                                                            {act.name}
                                                                        </div>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </td>
                                        )
                                        : null
                                }
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        ]
    }
}
TableWCP.propTypes = {
    config: PropTypes.object.isRequired,
    items: PropTypes.array.isRequired
};