import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class LeftMenu extends Component {
    constructor(props) {
        super(props);
        this.parMenu = this.parMenu.bind(this);
        this.childMenu = this.childMenu.bind(this);
    }
    parMenu() {
        const { auth } = this.props;
        let parent;
        if(auth.menu) {
            if(Array.isArray(auth.menu)){
                if(auth.menu.length > 0){
                    auth.menu.map((el) => {
                        if(el.apiurl === this.props.servicename) parent = el.id;
                    })
                    return auth.menu.map((el) => {
                        let hidden = el.config ? el.config.hidden ? el.config.hidden : false : false;
                        return el.apiurl === this.props.servicename && hidden === false
                            ?   <Link   key={`menuL${el.id}`}
                                        to={`/dashboard/${this.props.servicename}/${el.apiurl}`}
                                        // onClick={ ()=> this.urlToRedux() }
                                        className={`item ${ el.apiurl === this.props.compname ? 'active' : null }`}>
                                        {el.ptitle}
                                </Link>
                            :   null
                    })
                }
            }
        }
    }
    childMenu() {
        const { auth } = this.props;
        let parent;
        if(auth.menu) {
            if(Array.isArray(auth.menu)){
                if(auth.menu.length > 0){
                    auth.menu.map((el) => {
                        if(el.apiurl === this.props.servicename) parent = el.id;
                    })
                    return auth.menu.map((el) => {
                        let hidden = el.config ? el.config.hidden ? el.config.hidden : false : false;
                        return el.parentid === parent && el.typem !== 'getone' && hidden === false
                            ?   <Link   key={`menuL${el.id}`}
                                        to={`/dashboard/${this.props.servicename}/${el.apiurl}`}
                                        // onClick={ ()=> this.urlToRedux() }
                                        className={`item ${ el.apiurl === this.props.compname ? 'active' : null }`}>
                                        {el.ptitle}
                                </Link>
                            :   null
                    })
                }
            }
        }
    }
    render() {
        return (
            <div className='left_Menu'>
                <div className='ui vertical menu'>
                    {this.parMenu()}
                </div>
                <div className='ui vertical menu'>
                    {this.childMenu()}
                </div>
            </div>
        );
    }
}

LeftMenu.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(LeftMenu);
