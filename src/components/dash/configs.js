import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Configs extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <h2>Настройки</h2>
            </div>
        );
    }
}

Configs.propTypes = {
    list: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        list: state.list
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Configs);