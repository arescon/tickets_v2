import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import Pagination from 'rc-pagination';
import TableWCP from './TableWCP';
import Editor from '../../blocks/constr/editor';
import Checkbox from './../../blocks/globals/CheckForm';

import MenuEditor from '../../blocks/menu';
import Tiket from './../../blocks/tiket/index';
import TiketOne from './../../blocks/tiketOne/index';
import Profile from './../../blocks/profile/index';
import MethodEditor from '../../blocks/methodEditor';
import EditViewer from './../../blocks/methods/EditViewer';
import Notys from './../../blocks/notys/index';
import Newses from './../../blocks/news/index';
import Instruct from './../../blocks/instruct/index';
import Faqses from './../../blocks/faq/index';
import NewsOneBlock from './../../blocks/news/one';
import InstrOne from './../../blocks/instruct/one';

import * as authAction from '../../redux/actions/auth';

import Utils from '../../utils/utils';
import Api from '../../utils/';
const api = new Api();
const prefix = 'api';

class Template extends Component {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        this.state = ({
            intervalId: 0,
            redirect: false,
            redirectUrl: '',
            data: [],
            err: [],
            pagination: {
                pagenum: 1,
                pagesize: 20,
                foundcount: 0,
                substr: ''
            },
            servname: '',
            filters: {}
        });
    }
    initItem(string) {
        const { dispatch } = this.props;
        let filterQuery='',
            self = this;

        dispatch(authAction.handlerChangePreloader(true).bind(this));

        if(!Utils.isEmptyObject(this.state.filters)){
            let keys = Object.keys(this.state.filters);
            keys.forEach((el,i)=>{
                if(Array.isArray(this.state.filters[el])) {
                    if(this.state.filters[el].length>0) {
                        filterQuery+=`&${el}=[${this.state.filters[el]}]`
                    } else {
                        filterQuery+=`&${el}=`
                    }
                } else {
                    filterQuery+=`&${el}=${this.state.filters[el]}`
                }
            })
        }
        api.get(`${prefix}/${string}${filterQuery}`, {}, {
            s: (res) => {
                self.setState({
                    data: res.data,
                    pagination: update(this.state.pagination, {
                        ['foundcount']: { $set: res.data.foundcount ? res.data.foundcount : 0 }
                    })
                });
                dispatch(authAction.handlerChangePreloader(false).bind(this));
            }
        });
    }

    doubleClick(id, url) {
        let servicename = this.props.match.params.servicename;
        this.setState({
            redirectUrl: `${servicename}/${url}/${id}`
        },()=>{
            this.setState({
                redirect: true,
            })
        });
    }

    handlerActions(el) {
        let action = JSON.parse(el.currentTarget.dataset.el);
        let id = el.currentTarget.dataset.id;
        let func_name = el.currentTarget.dataset.func_name;
        switch (action.method) {
            case 'POST':
                api.post(`${prefix}/${action.url}`,{
                    id: id
                },{
                    s: ()=>{
                        this.initItem(`${func_name}?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);                    }
                });
                break;
            case 'PUT':
                api.put(`${prefix}/${action.url}?id=${id}`,{},{
                    s: ()=>{
                        this.initItem(`${func_name}?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);
                    }
                });
                break;
            case 'DELETE': {
                api.delete(`${prefix}/${action.url}?id=${id}`,{
                    s: ()=>{
                        this.initItem(`${func_name}?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);
                    }
                });
                break;
            };
        }
    }

    updateItem(data, method) {
        let compname = this.props.match.params.compname;
        let id = this.props.match.params.id;
        api.put(`${prefix}/${method}?id=${id}`, data, {
            s: (res)=> {
                // console.log(res);
            }
        });
    }

    saveItem(data, method) {
        let compname = this.props.match.params.compname;
        api.post(`${prefix}/${method}`, data, {
            s: (res)=> {
                // console.log(res);
            }
        });
    }

    goBack(url) {
        // console.log('back', url);
    }

    componentWillUpdate(nextProps, nextState) {
        if(nextProps.match.params === this.props.match.params.servicename) {
            nextState.pagination = {
                pagenum: 1,
                pagesize: 20,
                foundcount: 0,
                substr: ''
            }
        }
    }

    componentWillReceiveProps(nextProps) {
      this.setState({
        servname: nextProps.match.params.servicename
      })
      if(this.state.redirect === true)
      this.setState({
        redirect: false,
        redirectUrl: ''
      })
    }

    handlerChangePage(page){
        const { auth } = this.props;
        let servname = this.props.match.params.servicename;
        let compname = this.props.match.params.compname;
        let url = compname ? compname : servname
        let id = this.props.match.params.id;
        let method;
        auth.menu.map((el)=>{
            if (el.apiurl === url) {
                el.config ? el.config.func_table ? method = el.config.func_table :  method = null : method = null
            }
        });
        this.initItem(`${method}?pagenum=${page}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);

        this.setState({
            pagination: update(this.state.pagination,
                {
                    ['pagenum']: { $set: page }
                }
            )
        })
    }

    handlerChangePageDef(){
        this.setState({
            pagination: update(this.state.pagination,
                {
                    ['pagenum']: { $set: 1 }
                }
            )
        })
    }

    handlerSearch(ev){
        let valSearch = ev.currentTarget.value;
        this.setState({
            pagination: update(this.state.pagination,
                {
                    ['substr']: { $set: valSearch }
                }
            )
        })

    }

    handlerSearchKup(ev) {
        let servname = this.props.match.params.servicename;
        let compname = this.props.match.params.compname;
        let url = compname ? compname : servname;
        const { auth } = this.props;
        let method;
        if (ev.keyCode == 13) {
            auth.menu.map((el)=>{
                if (el.apiurl === url) {
                    el.config ? el.config.func_table ? method = el.config.func_table :  method = '' : method = ''
                }
            });
            this.initItem(`${method}?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);
        }
    }

    handlerFiltersDef(ev) {
        let keys = Object.keys(ev);
        let filters = new Object;
        keys.forEach(el=>{
            filters[el] = ev[el].default;
        })
        let pagination = {...this.state.pagination};
        pagination.substr = '';
        pagination.pagenum = 1;
        this.setState({
            filters: filters,
            pagination: pagination
        }, ()=>{
            const { auth } = this.props;
            let servname = this.props.match.params.servicename;
            let compname = this.props.match.params.compname;
            let url = compname ? compname : servname
            let method;
            auth.menu.map((el)=>{
                if (el.apiurl === url) {
                    el.config ? el.config.func_table ? method = el.config.func_table :  method = null : method = null
                }
            });
            this.initItem(`${method}?pagenum=${this.state.pagination.pagenum}&pagesize=${this.state.pagination.pagesize}&substr=${this.state.pagination.substr}`);
        });
    }

    handleChangeFilter(ev) {
        let filter = ev.target.dataset.filter;
        this.setState({
            filters: update(this.state.filters, {
                [`${filter}`]: { $set: this.state.filters[filter] ? !this.state.filters : true }
            })
        })
    }

    handleChangeFilterVal(obj) {
        this.setState({
            filters: update(this.state.filters, {
                [`${obj.filter}`]: { $set: obj.val }
            })
        })
    }

    handlerPrintReport() {
        let filterQuery='', self = this;
        if(!Utils.isEmptyObject(this.state.filters)){
            let keys = Object.keys(this.state.filters);
            keys.forEach((el,i)=>{
                if(Array.isArray(this.state.filters[el])) {
                    if(this.state.filters[el].length>0) {
                        filterQuery+=`&${el}=[${this.state.filters[el]}]`
                    } else {
                        filterQuery+=`&${el}=`
                    }
                } else {
                    filterQuery+=`&${el}=${this.state.filters[el]}`
                }
            })
        }
        window.open(`http://api.jelata.tech/fls/ticket_rep?filter=all${filterQuery}&substr=${this.state.pagination.substr}`);
    }

    handlerSizeTable(ev) { // сколько записей порказывать
        const { auth, match } = this.props;
        let servname = match.params.servicename;
        let compname = match.params.compname;
        let url = compname ? compname : servname
        let method;
        auth.menu.map((el)=>{
            if (el.apiurl === url) {
                el.config ? el.config.func_table ? method = el.config.func_table :  method = null : method = null
            }
        });
        let v = parseInt(ev.target.value);
        
        console.log('change size');

        this.initItem(`${method}?pagenum=${this.state.pagination.pagenum}&pagesize=${v}&substr=${this.state.pagination.substr}`);

        this.setState({
            pagination: update(this.state.pagination,
                {
                    ['pagesize']: { $set: v }
                }
            )
        })
    }

    scrollStep() {
        if (window.pageYOffset === 0) {
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }
    
    scrollToTop() {
        let intervalId = setInterval(this.scrollStep.bind(this), 16.66);
        this.setState({ intervalId: intervalId });
    }

    render() {
        const { auth } = this.props;
        let servname = this.props.match.params.servicename;
        let compname = this.props.match.params.compname;
        let url = compname ? compname : servname
        let id = this.props.match.params.id;

        let size_table = [5,10,20,40,100];

        if (this.state.redirect) return <Redirect to={`/dashboard/${this.state.redirectUrl}`}/>;
        if(auth.menu) {
            if(Array.isArray(auth.menu)){
                if(auth.menu.length > 0){
                    return auth.menu.map((el)=>{
                        if (el.apiurl === url) {
                            switch (el.mtype) {
                                case 'list':
                                    return (
                                        <div key={`${el.id}typems${el.typem}`}>
                                            <div className='ui segment'>
                                                <h3 className=''>{el.ptitle}</h3>
                                                { el.config ? el.config.new_url ? <Link to={`/dashboard/${servname}/${el.config ? el.config.new_url ? el.config.new_url : '' : ''}/0`} className='ui button' >Новая</Link> : null : null }
                                            </div>
                                            <div className='ui grid four column row'>
                                                <div className='column'>
                                                    <Pagination className='ant-pagination'
                                                                style={{margin: '4px 0'}}
                                                                current={ this.state.pagination.pagenum }
                                                                defaultPageSize={ this.state.pagination.pagesize }
                                                                total={ this.state.pagination.foundcount }
                                                                onChange={ (page)=> this.handlerChangePage(page) }
                                                                locale={{ items_per_page: '/странице',
                                                                    jump_to: 'Перейти', page: '',
                                                                    prev_page: 'Назад', next_page: 'Вперед',
                                                                    prev_5: 'Предыдущие 5', next_5: 'Следующие 5'
                                                                }} />
                                                </div>
                                                <div className='column'>
                                                    Сколько записей показывать
                                                    <select onChange={(ev)=>this.handlerSizeTable(ev)}>
                                                        {
                                                            size_table.map((el)=>{
                                                                return <option key={'elt_'+el} selected={this.state.pagination.pagesize == el} value={el}>{el}</option>
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                                <div className='column'>
                                                    <div className="ui grey inverted segment">
                                                        Количество заявок: { this.state.pagination ? this.state.pagination.foundcount : '' }
                                                    </div>
                                                </div>
                                                <div className='column'>
                                                    <div className='row'>
                                                        <div className='ui fluid icon input'>
                                                            <input  type='text'
                                                                    value={ this.state.pagination.substr }
                                                                    placeholder='Введите поисковый запрос...'
                                                                    onChange={ (ev)=> this.handlerSearch(ev) }
                                                                    onKeyUp={ (ev) => this.handlerSearchKup(ev) }
                                                            />
                                                            <i className='search icon'/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <TableWCP
                                                items={ this.state.data ? this.state.data.outjson ? this.state.data.outjson : [] : [] }
                                                config={ this.state.data ? this.state.data.config ? this.state.data.config : {} : {} }
                                                pagination={ this.state.pagination }

                                                filters={ this.state.filters }
                                                handlerFiltersDef={ev=>this.handlerFiltersDef(ev)}
                                                handleChangeFilter={ev=>this.handleChangeFilter(ev)}
                                                handleChangeFilterVal={obj=>this.handleChangeFilterVal(obj)}
                                                handlerChangePageDef={()=>this.handlerChangePageDef()}

                                                cur_servname={servname}
                                                before_servname={this.state.servname}
                                                url={url}
                                                handlerPrintReport={ev=>this.handlerPrintReport(ev)}

                                                menuItem={el}
                                                menuConfig={ el.config ? el.config : {} }
                                                method={ el.config ? el.config.func_table ? el.config.func_table : '' : '' }
                                                doubleUrl={el.config ? el.config.double_url ? el.config.double_url : '' : '' }
                                                currentLocation ={`/dashboard/${servname}`}
                                                actionFunc={(el)=> this.handlerActions(el)}
                                                actions={el.config ? el.config.actions ? el.config.actions : [] : []}
                                                rowDoubleClick={ (i, u)=> this.doubleClick(i, u) }
                                                initItems={ (string)=> this.initItem(string) }
                                            />
                                            <button
                                                title='Back to top'
                                                className='scroll' 
                                                onClick={ () => { this.scrollToTop(); }}
                                                style={{
                                                    margin: '0 50%',
                                                    border: 'none',
                                                    background: '#767676',
                                                    color: 'white',
                                                    padding: '10px 30px',
                                                    cursor: 'pointer',
                                                    marginBottom: '100px'
                                                }}
                                            >
                                                <span className=''>Наверх</span>
                                            </button>
                                        </div>
                                    );
                                case 'getOne':
                                    return (
                                        <div key={`${el.id}typems${el.typem}`}>
                                            <div className='ui segment'>
                                                <h3 className=''>{el.title}</h3>
                                                <Editor id = { this.props.match.params.id }
                                                        config = { this.state.data ? this.state.data.config ? this.state.data.config : {} : {} }
                                                        items = { this.state.data ? !Array.isArray(this.state.data.outjson) ? this.state.data.outjson : {} : {} }
                                                        method={ el.config ? el.config.method ? el.config.method : '' : '' }
                                                        currentLocation ={`/dashboard/${el.service}`}
                                                        initItems = { (string)=> this.initItem(string) }
                                                        updateItem = { (item, string)=> this.updateItem(item, string) }
                                                        saveItem = { (item, string)=> this.saveItem(item, string) } />
                                            </div>
                                        </div>
                                    );
                                    
                                case 'method':
                                    return (
                                        <div key={`${el.id}typems${el.typem}`}>
                                            <div className='ui segment'>
                                                <h3 className=''>{el.ptitle}</h3>
                                                <EditViewer id = { this.props.match.params.id }
                                                            config = { this.state.data ? this.state.data.config ? this.state.data.config : {} : {} }
                                                            item = { this.state.data ? !Array.isArray(this.state.data.outjson) ? this.state.data.outjson : {} : {} }
                                                            method={ el.config ? el.config.method ? el.config.method : '' : '' }
                                                            initItems = { (string)=> this.initItem(string) }
                                                            saveItem={ (item, string)=> this.saveItem(item, string) } />
                                            </div>
                                        </div>
                                    );

                                case 'menu':
                                    return (
                                        <div key={`${el.id}typems${el.typem}`}>
                                            <MenuEditor id={id} servname={servname} compname={compname}/>
                                        </div>
                                    )
                                case 'profile':
                                    return (
                                        <div key={`${el.id}typems${el.typem}`}>
                                            <Profile
                                                id = { this.props.match.params.id }
                                                params = { this.props.match.params }
                                                config = { this.state.data ? this.state.data.config ? this.state.data.config : {} : {} }
                                                item = { this.state.data ? !Array.isArray(this.state.data.outjson) ? this.state.data.outjson : {} : {} }
                                                method={ el.config ? el.config.method ? el.config.method : '' : '' }
                                                currentLocation ={`/dashboard/${el.service}`}
                                                initItems = { (string)=> this.initItem(string) }
                                                updateItem = { (item, string)=> this.updateItem(item, string) }
                                                saveItem = { (item, string)=> this.saveItem(item, string) }
                                            />
                                        </div>
                                    )
                                case 'ticket':
                                    return (
                                        <div key={`${el.id}typems${el.typem}`}>
                                            <Tiket
                                                id = { this.props.match.params.id }
                                                menuItem={el}
                                                auth={auth}
                                                params = { this.props.match.params }
                                                config = { this.state.data ? this.state.data.config ? this.state.data.config : {} : {} }
                                                item = { this.state.data ? !Array.isArray(this.state.data.outjson) ? this.state.data.outjson : {} : {} }
                                                method={ el.config ? el.config.method ? el.config.method : '' : '' }
                                                currentLocation ={`/dashboard/${el.service}`}
                                                initItems = { (string)=> this.initItem(string) }
                                                updateItem = { (item, string)=> this.updateItem(item, string) }
                                                saveItem = { (item, string)=> this.saveItem(item, string) }
                                            />
                                        </div>
                                    )
                                    break;
                                case 'ticketone':
                                    return (
                                        <div key={`${el.id}typems${el.typem}`}>
                                            <TiketOne
                                                    id = { this.props.match.params.id }
                                                    params = { this.props.match.params }
                                                    config = { this.state.data ? this.state.data.config ? this.state.data.config : {} : {} }
                                                    item = { this.state.data ? !Array.isArray(this.state.data.outjson) ? this.state.data.outjson : {} : {} }
                                                    method={ el.config ? el.config.method ? el.config.method : '' : '' }
                                                    currentLocation ={`/dashboard/${el.service}`}
                                                    initItems = { (string)=> this.initItem(string) }
                                                    updateItem = { (item, string)=> this.updateItem(item, string) }
                                                    saveItem = { (item, string)=> this.saveItem(item, string) }
                                                />
                                        </div>
                                    )
                                    break;
                                case 'info_one':
                                    return (
                                      <div key={`${el.id}typems${el.typem}`}>
                                        <NewsOneBlock id={ id } mtype={1} />
                                      </div>
                                    )
                                    break;
                                case 'faqs_one':
                                    return (
                                      <div key={`${el.id}typems${el.typem}`}>
                                        <NewsOneBlock id={ id } entype={2} />
                                      </div>
                                    )
                                    break;
                                case 'instr_one':
                                  return (
                                    <div key={`${el.id}typems${el.typem}`}>
                                      <InstrOne id={ id } />
                                    </div>
                                  )
                                  break;
                                case 'news':
                                  return (
                                    <div key={`${el.id}typems${el.typem}`}>
                                      <Newses />
                                    </div>
                                  )
                                  break;
                                case 'faqs':
                                  return (
                                    <div key={`${el.id}typems${el.typem}`}>
                                      <Faqses />
                                    </div>
                                  )
                                  break;
                                case 'instructions':
                                  return (
                                    <div key={`${el.id}typems${el.typem}`}>
                                      <Instruct />
                                    </div>
                                  )
                                  break;
                                case 'notys':
                                    return (
                                      <div key={`${el.id}typems${el.typem}`}>
                                        <Notys />
                                      </div>
                                    )
                                    break;
                                default:
                                    return (
                                      <div key={`${el.id}typems${el.typem}`}>
                                        Главная админки
                                      </div>
                                    )
                            }
                        }
                    });
                } else return null;
            } else return null;
        } else return null;
    }
}

Template.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Template);