import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Api from '../../utils/';
import Utils from '../../utils/utils';

import * as authAction from '../../redux/actions/auth';

const api = new Api();
const versia = 'v. 0.5';

class Header extends Component {
    constructor(props) {
      super(props);
      this.state = ({
          redirect: false,
          socket: {
            massages: 0,
            data: {}
          }
      });
      this.ws = undefined;
    }
    componentDidMount() {
      const { dispatch } = this.props;
      $('.ui.dropdown').dropdown();
      this.ws = new WebSocket("ws://api.jelata.tech/ws");
      let socket = this.ws;
      socket.addEventListener('open', () => {
        socket.send("1");
      });
      socket.addEventListener('message', (event) => {
        dispatch(authAction.handlerNotysData(JSON.parse(event.data)));
      });
      socket.addEventListener('close', (res) => {
          if (res.type == 'error') {
              Utils.showErr('Уведомления отключились, пожалуйста обновите страницу или перезайдите в портал', 5000);
          }
      });
    }
    logout() {
      api.get('atf/out', {}, {
        s: () => {
          this.setState({ redirect: true });
        }
      });
      this.setState({ redirect: true });
    }
    servMenu() {
        const { auth } = this.props;
        if (!Utils.isEmptyObject(auth.menu)) {
            return auth.menu.map((el) => {
              let hidden = el.config ? el.config.hidden ? el.config.hidden : false : false;
              if (el.parentid === 0 && hidden === false) {
                return (
                  <Link key={`menuRoot${el.id}`}
                        to={`/dashboard/${el.apiurl}`}
                        className={`item ${ el.apiurl === this.props.thisLocation ? 'active' : null }`}>
                    {el.ptitle}
                  </Link>
                );
              }
            });
        }
    }
    render() {
      const { auth } = this.props;
      if (this.state.redirect) return <Redirect to='/' />;
      return (
        <div className='header-top'>
          <Helmet>
            <meta charSet='utf-8' />
            <title>Панель администратора</title>
          </Helmet>
          <div className='ui secondary menu'>
            <Link to='/dashboard' className={`item ${ this.props.thisLocation ? null : 'active' }`}>Главная</Link>
            { this.servMenu() }
            <div className='right menu'>
              <span className='ui item'>
                { versia }
              </span>
              <div className='ui item'>
                <Link to='/notys'
                      className={`item ${ 'notys' === this.props.thisLocation ? 'active' : null }`}>
                    <i className='bell icon'></i>
                    <div className='floating ui red label my_labelk' style={{display: auth.notys.length > 0 ? 'block' : 'none'}}>{auth.notys.length}</div>
                </Link>
              </div>
              <div className='ui dropdown item'>
                <i className='settings icon'></i>
                <div className='menu'>
                  <Link className='item' to='/profile'>Линый кабинет</Link>
                  <a className='item log_out' onClick={ ()=> { this.logout() } }>
                    Выйти
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

Header.propTypes = {
    auth: PropTypes.any,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Header);
