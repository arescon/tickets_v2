import Api from '../../utils/';
const api = new Api();

export const GET_MENU = 'GET_MENU';
export const MENU_EDIT = 'GET_EDIT';
export const MENU_DELETE = 'MENU_DELETE';

export const GET_USER_INFO = 'GET_USER_INFO';
export const GO_TYPEM = 'GO_TYPEM';
export const CHANGE_PRELOADER = 'CHANGE_PRELOADER';

export const NOTYSY_DATA = 'NOTYSY_DATA';

export function handlerNotysData(data) {
  return (dispatch) => {
      dispatch({
          type: NOTYSY_DATA,
          payload: {
              data: data
          }
      });
  };
};

export function getMenuUser(collbacks) {
    return (dispatch) => {
        api.get('api/menu', {}, {
            s: (res) => {
                dispatch({
                    type: GET_MENU,
                    payload: {
                        data: res.data.outjson.menu,
                        dashboard: res.data.outjson.dashboard,
                        roles: res.data.outjson.roles,
                        error: {},
                        loading: false
                    }
                });
                collbacks ? collbacks.s ? collbacks.s() : null : null
            },
            e: (res) => {
              api.toLogin();
            }
        });
    };
};

export function handlerChangePreloader(status) {
    return (dispatch) => {
        dispatch({
            type: CHANGE_PRELOADER,
            payload: {
                loading: status
            }
        });
    };
};

export function handlerDeleteMenu(_id) {
    return (dispatch) => {
        dispatch({
            type: MENU_DELETE,
            payload: {
                id: _id
            }
        });
    };
};

export function handlerEditMenu(item_menu, _id) {
    return (dispatch) => {
        dispatch({
            type: MENU_EDIT,
            payload: {
                item: item_menu,
                id: _id
            }
        });
    };
};

export function go_auth(res) {
    return (dispatch) => {
        dispatch({
            type: GET_EDIT,
            payload: {
                user: res
            }
        });
    };
};

export function go_typem(res) {
    return (dispatch) => {
        dispatch({
            type: GO_TYPEM,
            payload: {
                typem: res
            }
        });
    };
};