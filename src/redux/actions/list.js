import Utils from '../../utils/';

export const GET_LIST = 'GET_LIST';

export function listRequest() {
    return (dispatch, getState) => {
        Utils.post('http://www.softomate.net/ext/employees/list.json')
            .then(res => {
                dispatch({
                    type: GET_LIST,
                    payload: res.data
                })
            });
    };
}
export function listDelete() {
    return (dispatch, getState) => {
        dispatch({
            type: GET_LIST,
            payload: {}
        })
    }
}