import {
    GET_LIST,
} from '../actions/list';

let initialState = {
    items: {},
    type: 0
};

export default function getList(state = initialState, action) {
    switch (action.type) {
        case GET_LIST: {
            return { ...state, items : action.payload };
        }
        default:
            return state;
    }
}