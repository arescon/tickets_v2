import Utils from '../../utils/utils';

import {
    GET_MENU,
    GET_USER_INFO,
    GO_TYPEM,
    MENU_DELETE,
    MENU_EDIT,
    CHANGE_PRELOADER,
    NOTYSY_DATA
} from '../actions/auth';

let initialState = {
    menu: {},
    error: {},
    loading: true,
    user: {},
    typem: '',
    dashboard: {},
    notys: []
};

export default function getList(state = initialState, action) {
  switch (action.type) {
    case GET_MENU: {
      return {
        ...state,
        menu: action.payload.data,
        roles: action.payload.roles,
        dashboard: action.payload.dashboard,
        error: action.payload.error,
        loading: action.payload.loading,
        date_upd: new Date(1)
      };
    }
    case MENU_DELETE: {
      let data = state.menu;
      let index = Utils.findIndexByKeyValue(state.menu,'id',action.payload.id)
      data.splice(index,1);
      return {
        ...state,
        menu: data
      };
    }
    case MENU_EDIT: {
      let data = state.menu;
      let index = Utils.findIndexByKeyValue(state.menu,'id',action.payload.item.id)
      if(index) {
        data[index] = action.payload.item;
      } else {
        data.push(action.payload.item);
      }
      return {
        ...state,
        menu: data
      };
    }
    case GET_USER_INFO: {
      return {
        ...state,
        user: action.payload.user,
      };
    }
    case GO_TYPEM: {
      return {
        ...state,
        typem: action.payload.typem,
      };
    }
    case CHANGE_PRELOADER: {
      return {
        ...state,
        loading: action.payload.loading,
      };
    }
    case NOTYSY_DATA: {
      return {
        ...state,
        notys: action.payload.data,
      };
    }
    default:
      return state;
  }
}